<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vifonic
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div class="row">
		<main id="main" class="site-main col-md-6 col-sm-12 col-md-push-3 col-sm-push-0" role="main">
			<div class="top-site-main">
				<div class="breadcrumbs-wrapper">
					<?php vifonic_breadcrumbs(); ?>
				</div>
			</div>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'single' ); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
		<?php get_sidebar() ?>
		<?php get_sidebar( 'second' ) ?>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
