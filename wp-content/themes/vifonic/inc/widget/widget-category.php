<?php
class vf_category_widget extends WP_Widget {
	
	function vf_category_widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget_category_ID', 'description' => 'A widget that show gallery' );

		/* Create the widget. */
		$this->WP_Widget( 'vf_category-widget', 'Vifonic - Category', $widget_ops);
	}
	 
	function widget($args, $instance) {
		global $post;
		$html = '';
		$html .= $args['before_widget'];
		if( !empty($instance['title']) ) {
			$html .= $args['before_title'];
			$html .= $instance['title'];
			$html .= $args['after_title'];
		}
		if( $instance['cat_ID'] ){
			$class = $instance['show_image'] ? 'has-thumbnail' : 'no-thumbnail';
			$html .= '<ul class="list-post-cat-id '. $class .'">';
			$args = array(
				'cat'				=> $instance['cat_ID'],
				'posts_per_page'	=> $instance['number_post']
			);
			$list_post =  get_posts( $args);
			if( $list_post ) : foreach( $list_post as $post ): setup_postdata($post);
				$html .= '<li>';
					if( ! $instance['show_image'] ){
						$html .= '<a href="'. get_permalink($post->ID) .'">'. get_the_title($post->ID) .'</a>';
					}
				$html .= '</li>';
			endforeach; endif;
			$html .= '</ul>';
		}
		echo force_balance_tags($html);
	}
 
	function update($new_instance, $old_instance) {
		return $new_instance;
	}
 
	function form($instance) {
		?><br/>
		<label>Title:
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>" style="width:100%" /></label>
		<br /><br />
		<label>Chọn danh mục:</label><br />
		<?php
		global $RS;
		$category_ids = get_all_category_ids();

		?>
		<select style="width:100%" name="<?php echo esc_attr($this->get_field_name( 'cat_ID' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'cat_ID' )); ?>">
			<?php
				foreach( $category_ids as $catid ){
					if( $instance['cat_ID'] == $catid ){
						$selected = 'selected';
					}else{
						$selected = '';
					}
					echo '<option value="'.$catid.'" '. $selected .' >'.get_cat_name($catid).'</option>';
				}
			?>
		</select>
			<br /><br />
		<label>Nhập số bài viết hiển thị:</label><br />
		<input type="text" id="<?php echo esc_attr($this->get_field_id( 'number_post' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number_post' )); ?>" value="<?php echo isset($instance['number_post']) ? esc_attr($instance['number_post']) : ''; ?>" style="width:100%" />
		<br/> <br/>
		<label>Hiển thị hình ảnh:
		<input type="checkbox" id="<?php echo esc_attr($this->get_field_id( 'show_image' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_image' )); ?>" <?php echo $instance['show_image'] ? 'checked' : '' ?> /></label>
		<?php
	}
	
}

add_action( 'widgets_init', 'create_category_widget' );

function create_category_widget(){
	return register_widget("vf_category_widget");
}