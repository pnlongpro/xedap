<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/9/2015
 * Time: 3:16 PM
 */
function add_contact_fields(){
	add_meta_box('show-shiping-field','Nội dung đơn hàng','show_shiping_field','shiping','normal','high',array());
}
function show_shiping_field($post){
	echo $post->post_content;

}
add_action('add_meta_boxes','add_contact_fields');

if(isset($_POST['checkout'])){
	$html = '';
	$html .= '<p><strong>Họ Tên:</strong> '. $_POST['txt-name'] .'';
	$html .= '<p><strong>Email:</strong> '. $_POST['txt-email'] .'';
	$html .= '<p><strong>Số điện thoại:</strong> '. $_POST['txt-phone'] .'';
	$html .= '<p><strong>Địa chỉ:</strong> '. $_POST['txt-address'] .'';
	$html .= '<p><strong>Nội dung: </strong> '. $_POST['txt-messages'] .'';
	$html .= '<p><strong>Sản phẩm:</strong> '. $_POST['txt-product'] .'';
	$html .= '<p><strong>Ngày đặt hàng:</strong> '. date("j/n/Y") .'';

	$post_content = $html;
	$date = date("j/n/Y");
	if( isset($error) && $error != '' ){
		echo '<p style="padding: 15px;" class="bg-danger">';
		foreach( $error as $value ){
			echo $value .'<br />';
		}
		echo '</p>';
	}else {
		$post_id = wp_insert_post( array(
			'post_title'   => $_POST['txt-product'] . ' - ' . $_POST['txt-name'] . ' - ' . $date . '',
			'post_type'    => 'shiping',
			'post_status'  => 'pending',
			'post_content' => $post_content
		) );

		update_field( 'contact_name', $_POST['txt-name'], $post_id );
		update_field( 'contact_address', $_POST['txt-address'], $post_id );
		update_field( 'contact_phone', $_POST['txt-phone'], $post_id );
		update_field( 'contact_email', $_POST['txt-email'], $post_id );
		update_field( 'contact_messeges', $_POST['txt-messages'], $post_id );
		update_field( 'contact_date', $date, $post_id );
		update_field( 'contact_product', $_POST['txt-product'], $post_id );
	}
}

//Function that Adds a 'Views' Column to your Posts tab in WordPress Dashboard.
function post_column_views($newcolumn){
	//Retrieves the translated string, if translation exists, and assign it to the 'default' array.
//	$newcolumn['contact_name'] = __('Họ và Tên');
	$newcolumn['contact_note'] = __('Ghi chú đơn hàng');
	return $newcolumn;
}
//Function that Populates the 'Views' Column with the number of views count.
function post_custom_column_views($column_name, $post_ID){
	if($column_name === 'contact_note'){
		echo get_field('contact_note',$post_ID);;
	}
}
add_filter( 'manage_shiping_posts_columns' , 'post_column_views' );

add_action('manage_shiping_posts_custom_column', 'post_custom_column_views',10,2);

function my_manage_columns( $columns ) {
//	unset($columns['title']);
	unset($columns['date']);
	return $columns;
}

function my_column_init() {
	add_filter( 'manage_shiping_posts_columns' , 'my_manage_columns' );
}
add_action( 'admin_init' , 'my_column_init' );