<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/4/2015
 * Time: 2:57 PM
 */
// Register Custom Post Type

function custom_service_post_type() {



	$labels = array(

		'name'                => _x( 'product', 'Post Type General Name', 'vifonic' ),
		'singular_name'       => _x( 'Sản phẩm', 'Post Type Singular Name', 'vifonic' ),
		'menu_name'           => __( 'Sản phẩm', 'vifonic' ),
		'name_admin_bar'      => __( 'Sản phẩm', 'vifonic' ),
		'parent_item_colon'   => __( 'Sản phẩm cha', 'vifonic' ),
		'all_items'           => __( 'Tất cả Sản phẩm', 'vifonic' ),
		'add_new_item'        => __( 'Thêm mới Sản phẩm', 'vifonic' ),
		'add_new'             => __( 'Thêm mới', 'vifonic' ),
		'new_item'            => __( 'Sản phẩm mới', 'vifonic' ),
		'edit_item'           => __( 'Sửa Sản phẩm', 'vifonic' ),
		'update_item'         => __( 'Cập nhật', 'vifonic' ),
		'view_item'           => __( 'Xem Sản phẩm', 'vifonic' ),
		'search_items'        => __( 'Tìm kiếm', 'vifonic' ),
		'not_found'           => __( 'Không tìm thấy', 'vifonic' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'vifonic' ),
	);
	$args = array(
		'label'               => __( 'Sản phẩm', 'vifonic' ),
		'description'         => __( 'Sản phẩm', 'vifonic' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail','comments' ),
		'taxonomies'          => array(  ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-cart',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array('slug' => 'san-pham'),
	);
	register_post_type( 'product', $args );



}



// Hook into the 'init' action

add_action( 'init', 'custom_service_post_type', 0 );



if ( ! function_exists( 'product_cat_taxonomy' ) ) {

// Register Custom Taxonomy
	function product_cat_taxonomy() {

		$labels = array(
			'name'                       => _x( 'Danh mục sản phẩm', 'Taxonomy General Name', 'vifonic' ),
			'singular_name'              => _x( 'Danh mục sản phẩm', 'Taxonomy Singular Name', 'vifonic' ),
			'menu_name'                  => __( 'Danh mục sản phẩm', 'vifonic' ),
			'all_items'                  => __( 'All Items', 'vifonic' ),
			'parent_item'                => __( 'Parent Item', 'vifonic' ),
			'parent_item_colon'          => __( 'Parent Item:', 'vifonic' ),
			'new_item_name'              => __( 'New Item Name', 'vifonic' ),
			'add_new_item'               => __( 'Add New Item', 'vifonic' ),
			'edit_item'                  => __( 'Edit Item', 'vifonic' ),
			'update_item'                => __( 'Update Item', 'vifonic' ),
			'view_item'                  => __( 'View Item', 'vifonic' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'vifonic' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'vifonic' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'vifonic' ),
			'popular_items'              => __( 'Popular Items', 'vifonic' ),
			'search_items'               => __( 'Search Items', 'vifonic' ),
			'not_found'                  => __( 'Not Found', 'vifonic' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'           => array( 'slug' => 'danh-muc-san-pham' ),
		);
		register_taxonomy( 'product_cat', array( 'product' ), $args );

	}
	add_action( 'init', 'product_cat_taxonomy', 0 );

}




function custom_register_post_type() {
	$labels = array(
		'name'                => _x( 'Đơn hàng', 'Đơn hàng', 'vifonic' ),
		'singular_name'       => _x( 'Đơn hàng', 'Post Type Singular Name', 'vifonic' ),
		'menu_name'           => __( 'Đơn hàng', 'vifonic' ),
		'name_admin_bar'      => __( 'Đơn hàng', 'vifonic' ),
		'all_items'           => __( 'Đơn hàng', 'vifonic' ),
		'add_new_item'        => __( 'Thêm mới', 'vifonic' ),
		'add_new'             => __( 'Thêm mới', 'vifonic' ),
		'new_item'            => __( 'Đăng ký mới', 'vifonic' ),
		'edit_item'           => __( 'Sửa thông tin ', 'vifonic' ),
		'update_item'         => __( 'Cập nhật', 'vifonic' ),
		'view_item'           => __( 'Xem thông tin ', 'vifonic' ),
		'search_items'        => __( 'Tìm kiếm', 'vifonic' ),
		'not_found'           => __( 'Không tìm thấy', 'vifonic' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'vifonic' ),
	);

	$args = array(
		'label'               => __( 'Đơn hàng', 'vifonic' ),
		'description'         => __( 'Đơn hàng', 'vifonic' ),
		'labels'              => $labels,
		'supports'            => array( 'title', ),
		'taxonomies'          => array(  ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-media-document',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	register_post_type( 'shiping', $args );
}
// Hook into the 'init' action

add_action( 'init', 'custom_register_post_type', 0 );