﻿<?php
/**
 * Created by PhpStorm.
 * User: Phan Long
 * Date: 6/23/2015
 * Time: 10:49 PM
 */
rs::addOptionTab(array(
	'title' => 'Tùy Chọn Chung',
	'name'  => 'rs-general-options',
	'icon'  => 'dashicons-admin-generic',
	'controls' => array(
		array(
			'name' => 'logo',
			'type' => 'image',
			'label' => 'Logo'
		),
		array(
			'name' => 'banner',
			'type' => 'image',
			'label' => 'Banner'
		),
		array(
			'name' => 'slider',
			'type' => 'text',
			'label' => 'Slider short code'
		),
//		array(
//			'name' => 'slogan',
//			'type' => 'text',
//			'label' => 'Slogan'
//		),
		array(
			'name' => 'favicon',
			'type' => 'image',
			'label' => 'Favicon'
		),
//		array(
//			'name' => 'hotline',
//			'type' => 'text',
//			'label' => 'Hotline'
//		),
//		array(
//			'name' => 'phone',
//			'type' => 'text',
//			'label' => 'Số điện thoại bàn'
//		),
//		array(
//			'name' => 'email',
//			'type' => 'text',
//			'label' => 'Email'
//		)
	)
));
