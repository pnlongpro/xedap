<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package vifonic
 */

/**
 * Hàm Phân Trang
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
if ( !function_exists( 'vifonic_paging_nav' ) ) :

	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function vifonic_paging_nav() {
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		}
		$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$pagenum_link = html_entity_decode( get_pagenum_link() );

		$query_args = array();
		$url_parts  = explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$pagenum_link = esc_url( remove_query_arg( array_keys( $query_args ), $pagenum_link ) );
		$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

		$format = $GLOBALS['wp_rewrite']->using_index_permalinks() && !strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

		// Set up paginated links.
		$links = paginate_links( array(
			'base'      => $pagenum_link,
			'format'    => $format,
			'total'     => $GLOBALS['wp_query']->max_num_pages,
			'current'   => $paged,
			'mid_size'  => 1,
			'add_args'  => array_map( 'urlencode', $query_args ),
			'prev_text' => __( '<i class="fa fa-long-arrow-left"></i>', 'vifonic' ),
			'next_text' => __( '<i class="fa fa-long-arrow-right"></i>', 'vifonic' ),
			'type'      => 'list'
		) );

		if ( $links ) :
			?>
			<div class="pagination loop-pagination">
				<?php echo '<span> Trang </span>'
				?>
				<?php echo ent2ncr( $links ); ?>
			</div>
			<!-- .pagination -->
		<?php
		endif;
	}
endif;

/*****************************----end vifonic_paging_nav---*****************/


/*
  *Nex, previous post for single
  *
  */
if ( !function_exists( 'vifonic_post_nav' ) ) :

	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function vifonic_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( !$next && !$previous ) {
			return;
		}
		?>
		<nav class="navigation post-navigation" role="navigation">
			<div class="nav-links">
				<?php
				previous_post_link( '<div class="nav-previous">%link</div>', _x( '<i class="fa fa-long-arrow-left"></i>', 'Previous post link', 'thim' ) );
				next_post_link( '<div class="nav-next">%link</div>', _x( '<i class="fa fa-long-arrow-right"></i>', 'Next post link', 'thim' ) );
				?>
				<div class="clear"></div>
			</div>
			<!-- .nav-links -->
		</nav><!-- .navigation -->
	<?php
	}

endif;

/*****************************----end vifonic_post_nav---*****************/

 /*
  *
  *get_breadcrumbs
  *
  */
function vifonic_breadcrumbs() {
	global $wp_query, $post;
	// Start the UL
	$delimiter = "<i class='arrow-breadcrumb'></i>";
	echo '<ul class="woocommerce-breadcrumb breadcrumb" itemprop="breadcrumb">';
	echo '<li><a href="' . home_url() . '" class="home">' . __( "Trang Chủ", 'vifonic' ) . '</a>' . $delimiter . '</li>';
	if ( is_category() ) {
		$catTitle = single_cat_title( "", false );
		$cat      = get_cat_ID( $catTitle );
		echo '<li>' . get_category_parents( $cat, TRUE, $delimiter ) . '</li>';
	} elseif ( is_archive() && !is_category() ) {
		if( get_post_type() == 'product' ) {
			if ( is_tax( 'product_cat' ) ) {
				$current_term = $wp_query->get_queried_object();
				$ancestors    = array_reverse( get_ancestors( $current_term->term_id, 'product_cat' ) );
				foreach ( $ancestors as $ancestor ) {
					$ancestor = get_term( $ancestor, 'product_cat' );
					echo '<li><a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a></li>';
				}
				echo '<li>' . esc_html( $current_term->name ) . '</li>';
			} else {
				echo '<li>' . _e( 'Sản phẩm', 'vifonic' ) . '</li>';
			}
		}else{
			echo '<li>' . _e( "Archives", "vifonic" ) . '</li>';
		}

	} elseif ( is_search() ) {
		echo '<li>' . _e( "Search Result", "vifonic" ) . '</li>';
	} elseif ( is_404() ) {
		echo '<li>' . _e( "404 Not Found", "vifonic" ) . '</li>';
	} elseif ( is_single( $post ) ) {
		if ( get_post_type() == 'post' ) {
			$category    = get_the_category();
			$category_id = get_cat_ID( $category[0]->cat_name );
			echo '<li>' . get_category_parents( $category_id, TRUE, $delimiter ) . '</li>';
			echo '<li>' . the_title( '', '', FALSE ) . '</li>';
		} elseif( get_post_type() == 'product' ){
			echo '<li> Sản phẩm </li>';
			echo '<li>' . the_title( '', '', FALSE ) . '</li>';
		}else {
			echo '<li>' . get_post_type() . '</li>';
			echo '<li>' . get_the_title() . '</li>';
		}
	} elseif ( is_page() ) {
		$post = $wp_query->get_queried_object();
		if ( $post->post_parent == 0 ) {
			echo "<li><a href='#'>" . the_title( '', '', FALSE ) . "</a>" . $delimiter . "</li>";
		} else {
			$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
			array_push( $ancestors, $post->ID );

			foreach ( $ancestors as $ancestor ) {
				if ( $ancestor != end( $ancestors ) ) {
					echo '<li><a href="' . get_permalink( $ancestor ) . '">' . strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) . '</a>' . $delimiter . '</li>';
				} else {
					echo '<li>' . strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) . '</li>';
				}
			}
		}
	} elseif ( is_attachment() ) {
		$parent = get_post( $post->post_parent );
		if ( $parent->post_type == 'page' || $parent->post_type == 'post' ) {
			$cat = get_the_category( $parent->ID );
			$cat = $cat[0];
			echo '<li>' . get_category_parents( $cat, true, $delimiter ) . '</li>';
		}

		echo '<li><a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a>' . $delimiter . '</li>';
		echo '<li>' . get_the_title() . '</li>';
	}
	// End the UL
	echo "</ul>";
}

/*****************************----end get_breadcrumbs---*****************/
