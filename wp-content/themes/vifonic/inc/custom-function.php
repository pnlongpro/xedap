<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 8/18/2015
 * Time: 2:18 PM
 */

/*******************************************************/
/*
 * get excerpt
 */
if( !function_exists( 'excerpt' ) ) :
	function excerpt( $limit ) {
		$content = get_the_excerpt();
		$content = apply_filters( 'the_content', $content );
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = explode( ' ', $content, $limit );
		array_pop( $content );
		$content = implode( " ", $content );
		$content = '<div class="excerpt">' . $content . '</div>';

		return $content;
	}
endif;
/****************************----end get excerpt---***************************/

/*******************************************************
 *
 * get excerpt
 */

/****************************----end get excerpt---***************************/

/************ List Comment ***************/
if ( !function_exists( 'thim_comment' ) ) {
	function thim_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		//extract( $args, EXTR_SKIP );
		if ( 'div' == $args['style'] ) {
			$tag       = 'div';
			$add_below = 'comment';
		} else {
			$tag       = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo ent2ncr( $tag ) ?> <?php comment_class( 'description_comment' ) ?> id="comment-<?php comment_ID() ?>">
		<?php
		if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
			//echo comment_author_email( comment_ID() );
		}
		?>
		<div
			class="author"><?php printf( __( '<span class="author-name">%s</span>', 'vifonic' ), get_comment_author_link() ) ?></div>
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php _e( 'Bình luận của bạn đang chờ kiểm duyệt.', 'vifonic' ) ?></em>
		<?php endif; ?>
		<div class="comment-extra-info">
			<div class="date" itemprop="commentTime"><?php printf( get_comment_date(), get_comment_time() ) ?></div>
			<?php comment_reply_link( array_merge( $args, array(
				'add_below' => $add_below,
				'depth'     => $depth,
				'max_depth' => $args['max_depth']
			) ) ) ?>
			<?php edit_comment_link( __( 'Sửa', 'vifonic' ), '', '' ); ?>
		</div>
		<div class="message">
			<?php comment_text() ?>
		</div>
		<div class="clear"></div>
	<?php
	}
}
/************end list comment************/