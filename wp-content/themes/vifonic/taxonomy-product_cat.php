<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/9/2015
 * Time: 4:21 PM
 */
//Template Name: Sản phẩm
get_header();
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
?>

<div id="primary" class="content-area">
	<div class="top-site-main">
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php vifonic_breadcrumbs(); ?>
			</div>
		</div>
	</div>
	<div class="container">
	<div class="row">
		<main id="main" class="site-main col-md-9 col-sm-12" role="main">
			<div class="entry-header">
				<h2><?php the_title() ?></h2>
				<?php
					$description = get_field('product_cat_description',$queried_object);
				if( $description ) :
				?>
				<div class="descroption">
					<?php echo $description  ?>
				</div>
				<?php endif;  ?>
				<hr />
			</div>
			<div class="list-product row">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-md-4 col-sm-4 col-xs-12">
					<?php get_template_part( 'template-parts/content', 'product' ); ?>
					</div>
				<?php endwhile; // End of the loop.
				vifonic_paging_nav();
				// ?>
			</div>
		</main><!-- #main -->
		<div class="col-md-3 col-sm-12 filter">
			<div class="sidebar-filter">
				<div class="filter-widget filter-price">
					<?php
					$args = array(
						'post_type' => 'page', /* or just 'post' */
						'meta_query' => array(
							array(
								'key' => '_wp_page_template',
								'value' => 'pagetemplate/template-filter.php', /* your template file name */
								'compare' => '='
							)
						)
					);
					$pages = get_posts( $args );
					$link = get_permalink($pages[0]);
					wp_reset_postdata();
					?>
					<form action="<?php echo $link; ?>" id="filter-form" method="get">
						<h3>Khoảng giá</h3>
						<ul>
							<li>
								<label>
									<input name="price" <?php if( $_GET['price'] == "8000000,10000000" ){echo
									'checked';}
									?>
										   value="8000000,10000000"
										   type="radio" />
									Từ 8 - 10 triệu
								</label>

							</li>
							<li>
								<label>
									<input name="price" <?php if( $_GET['price'] == "10000000,12000000" ){echo
									'checked';}
									?>
										   value="10000000,12000000"
										   type="radio" />
									Từ 10 - 12 triệu
								</label>
							</li>
							<li>
								<label>
									<input name="price" <?php if( $_GET['price'] == "12000000,14000000" ){echo
									'checked';}
									?>
										   value="12000000,14000000"
										   type="radio" />
									Từ 12 - 14 triệu
								</label>
							</li>
							<li>
								<label>
									<input name="price" <?php if( $_GET['price'] == "14000000,16000000" ){echo
									'checked';}
									?>
										   value="14000000,16000000"
										   type="radio" />
									Từ 14 - 16 triệu
								</label>
							</li>
							<li>
								<label>
									<input name="price" <?php if( $_GET['price'] == "16000000,18000000" ){echo
									'checked';}
									?>
										   value="16000000,18000000"
										   type="radio" />
									Từ 16 - 18 triệu
								</label>
							</li>
						</ul>
						<h3>Hãng sản xuất</h3>
						<ul>
							<?php
								$list_term = get_terms('product_cat');
								if( $list_term ) {
									foreach( $list_term as $value ){
										?>
										<li>
											<label>
												<input name="category" <?php
												if( $_GET['category'] == $value->slug )
												{echo 'checked';}
												?>
													   value="<?php echo $value->slug ?>"
													   type="radio" />
												<?php echo $value->name ?>
											</label>

										</li>
										<?php
									}
								}
							?>
						</ul>
					</form>

				</div>
			</div>
		</div>
		<?php //get_sidebar( 'second' ) ?>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
