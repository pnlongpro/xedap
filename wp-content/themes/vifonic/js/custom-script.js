
/**
 * Created by PhanLong on 9/4/2015.
 */
jQuery(function($){

	$(document).ready(function() {
		if (typeof jQuery.fn.retina !== 'undefined') {
			if (jQuery().retina) {
				$(".retina").retina({preload: true})
			}
		}
		$('#filter-form input').click(function(){
			$('#filter-form').submit();
		});
		var adminH = 0;
		adminH = $('#wpadminbar').height();
		var menuH = $('#masthead .navbar-default').height();
		$('#masthead .navbar-default').sticky({topSpacing:0});

		//$('.product-detail .nav-tabs').sticky({topSpacing:adminH+menuH});
		$(window).load(function() {
			// The slider being synced must be initialized first
			if (typeof jQuery.fn.flexslider !== 'undefined') {
				$('#carousel').flexslider({
					animation    : "slide",
					controlNav   : false,
					animationLoop: false,
					slideshow    : false,
					itemWidth    : 150,
					itemMargin   : 10,
					asNavFor     : '#slider-product'
				});

				$('#slider-product').flexslider({
					animation    : "slide",
					controlNav   : false,
					animationLoop: false,
					slideshow    : false,
					sync         : "#carousel"
				});
			}
		});
		var owl = $(".feature-product .owl-carousel");
		if (typeof jQuery.fn.owlCarousel !== 'undefined') {
			owl.owlCarousel({
				itemsCustom: [
					[0, 1],
					[480, 2],
					[680, 2],
					[980, 3],
					[1200, 4],
				],
				//items:3,
				loop:true,
				margin:15,
				autoPlay:false,
				autoplayTimeout:1000,
				autoplayHoverPause:true,
				autoplaySpeed:5000
			});
			owl.trigger('owl.play',6000);
			$(".customNavigation .next").click(function () {
				owl.trigger('autoplay.play.owl',[1000])
				owl.trigger('owl.next');
			})
			$(".customNavigation .prev").click(function () {
				owl.trigger('owl.prev');
				//owl.trigger('autoplay.stop.owl') // This does not work.
			})
		}

		var owl1 = $(".list_partner");
		if (typeof jQuery.fn.owlCarousel !== 'undefined') {
			owl1.owlCarousel({
				itemsCustom: [
					[0, 1],
					[480, 2],
					[680, 2],
					[980, 3],
					[1200, 4],
				],
				//items:3,
				loop:true,
				margin:15,
				autoPlay:false,
				autoplayTimeout:1000,
				autoplayHoverPause:true,
				autoplaySpeed:5000
			});
			//owl.trigger('owl.play',6000);
			//$(".customNavigation .next").click(function () {
			//	owl.trigger('autoplay.play.owl',[1000])
			//	owl.trigger('owl.next');
			//})
			//$(".customNavigation .prev").click(function () {
			//	owl.trigger('owl.prev');
			//	//owl.trigger('autoplay.stop.owl') // This does not work.
			//})
		}
		$('.top-menu').slicknav({
			prependTo:'.main-navigation .container',
			allowParentLinks: true,
			duration: 1000
		});
	});


});