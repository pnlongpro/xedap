<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/18/2015
 * Time: 11:02 PM
 */
?>
<div id="search-form-wrapper" class="search-form">
	<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control" placeholder="Nhập từ khóa tìm kiếm...">
		<button type="submit" class="search-submit btn btn-default" role="button"><i class="fa fa-search"></i></button>
	</form>
	<div class="clear"></div>
</div>