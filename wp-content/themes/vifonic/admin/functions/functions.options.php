<?php

add_action( 'init', 'of_options' );
if ( ! function_exists( 'of_options' ) ) {
	function of_options() {
		//Access the WordPress Categories via an Array
		$of_categories     = array();
		$of_categories_obj = get_categories( 'hide_empty=0' );
		foreach ( $of_categories_obj as $of_cat ) {
			$of_categories[$of_cat->cat_ID] = $of_cat->cat_name;
		}
		$categories_tmp = array_unshift( $of_categories, "Select a category:" );

		//Access the WordPress Pages via an Array
		$of_pages     = array();
		$of_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
		foreach ( $of_pages_obj as $of_page ) {
			$of_pages[$of_page->ID] = $of_page->post_name;
		}
		$of_pages_tmp = array_unshift( $of_pages, "Select a page:" );

		//Testing
		$of_options_select = array( "one", "two", "three", "four", "five" );
		$of_options_radio  = array( "one" => "One", "two" => "Two", "three" => "Three", "four" => "Four", "five" => "Five" );

		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		(
			"disabled" => array(
				"placebo"     => "placebo", //REQUIRED!
				"block_one"   => "Block One",
				"block_two"   => "Block Two",
				"block_three" => "Block Three",
			),
			"enabled"  => array(
				"placebo"    => "placebo", //REQUIRED!
				"block_four" => "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets     = array();

		if ( is_dir( $alt_stylesheet_path ) ) {
			if ( $alt_stylesheet_dir = opendir( $alt_stylesheet_path ) ) {
				while ( ( $alt_stylesheet_file = readdir( $alt_stylesheet_dir ) ) !== false ) {
					if ( stristr( $alt_stylesheet_file, ".css" ) !== false ) {
						$alt_stylesheets[] = $alt_stylesheet_file;
					}
				}
			}
		}


		//Background Images Reader
		$bg_images_path = get_stylesheet_directory() . '/images/bg/'; // change this to where you store your bg images
		$bg_images_url  = get_template_directory_uri() . '/images/bg/'; // change this to where you store your bg images
		$bg_images      = array();

		if ( is_dir( $bg_images_path ) ) {
			if ( $bg_images_dir = opendir( $bg_images_path ) ) {
				while ( ( $bg_images_file = readdir( $bg_images_dir ) ) !== false ) {
					if ( stristr( $bg_images_file, ".png" ) !== false || stristr( $bg_images_file, ".jpg" ) !== false ) {
						natsort( $bg_images ); //Sorts the array into a natural order
						$bg_images[] = $bg_images_url . $bg_images_file;
					}
				}
			}
		}


		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/

		//More Options
		$uploads_arr      = wp_upload_dir();
		$all_uploads_path = $uploads_arr['path'];
		$all_uploads      = get_option( 'of_uploads' );
		$other_entries    = array( "Select a number:", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19" );
		$body_repeat      = array( "no-repeat", "repeat-x", "repeat-y", "repeat" );
		$body_pos         = array( "top left", "top center", "top right", "center left", "center center", "center right", "bottom left", "bottom center", "bottom right" );

		// Image Alignment radio box
		$of_options_thumb_align = array( "alignleft" => "Left", "alignright" => "Right", "aligncenter" => "Center" );

		// Image Links to Options
		$of_options_image_link_to = array( "image" => "The Image", "post" => "The Post" );

		$font_weight = array(
			'normal'  => 'Normal',
			'bold'    => 'Bold',
			'lighter' => 'Lighter',
			'100'     => '100',
			'200'     => '200',
			'300'     => '300',
			'400'     => '400',
			'500'     => '500',
			'600'     => '600',
			'700'     => '700',
			'800'     => '800',
			'900'     => '900',
			'inherit' => 'Inherit'
		);

		$font_style = array(
			'normal'  => 'Normal',
			'italic'  => 'Italic',
			'oblique' => 'Oblique',
			'inherit' => 'Inherit'
		);

		$text_transform = array(
			'capitalize' => 'Capitalize',
			'inherit'    => 'Inherit',
			'lowercase'  => 'Lowercase',
			'none'       => 'None',
			'uppercase'  => 'Uppercase'
		);

		$font_sizes = array(
			'10' => '10',
			'11' => '11',
			'12' => '12',
			'13' => '13',
			'14' => '14',
			'15' => '15',
			'16' => '16',
			'17' => '17',
			'18' => '18',
			'19' => '19',
			'20' => '20',
			'21' => '21',
			'22' => '22',
			'23' => '23',
			'24' => '24',
			'25' => '25',
			'26' => '26',
			'27' => '27',
			'28' => '28',
			'29' => '29',
			'30' => '30',
			'31' => '31',
			'32' => '32',
			'33' => '33',
			'34' => '34',
			'35' => '35',
			'36' => '36',
			'37' => '37',
			'38' => '38',
			'39' => '39',
			'40' => '40',
			'41' => '41',
			'42' => '42',
			'43' => '43',
			'44' => '44',
			'45' => '45',
			'46' => '46',
			'47' => '47',
			'48' => '48',
			'49' => '49',
			'50' => '50',
		);

		$google_fonts = array(
			"0"                        => "Select Font",
			"ABeeZee"                  => "ABeeZee",
			"Abel"                     => "Abel",
			"Abril Fatface"            => "Abril Fatface",
			"Aclonica"                 => "Aclonica",
			"Acme"                     => "Acme",
			"Actor"                    => "Actor",
			"Adamina"                  => "Adamina",
			"Advent Pro"               => "Advent Pro",
			"Aguafina Script"          => "Aguafina Script",
			"Akronim"                  => "Akronim",
			"Aladin"                   => "Aladin",
			"Aldrich"                  => "Aldrich",
			"Alegreya"                 => "Alegreya",
			"Alegreya SC"              => "Alegreya SC",
			"Alex Brush"               => "Alex Brush",
			"Alfa Slab One"            => "Alfa Slab One",
			"Alice"                    => "Alice",
			"Alike"                    => "Alike",
			"Alike Angular"            => "Alike Angular",
			"Allan"                    => "Allan",
			"Allerta"                  => "Allerta",
			"Allerta Stencil"          => "Allerta Stencil",
			"Allura"                   => "Allura",
			"Almendra"                 => "Almendra",
			"Almendra Display"         => "Almendra Display",
			"Almendra SC"              => "Almendra SC",
			"Amarante"                 => "Amarante",
			"Amaranth"                 => "Amaranth",
			"Amatic SC"                => "Amatic SC",
			"Amethysta"                => "Amethysta",
			"Anaheim"                  => "Anaheim",
			"Andada"                   => "Andada",
			"Andika"                   => "Andika",
			"Angkor"                   => "Angkor",
			"Annie Use Your Telescope" => "Annie Use Your Telescope",
			"Anonymous Pro"            => "Anonymous Pro",
			"Antic"                    => "Antic",
			"Antic Didone"             => "Antic Didone",
			"Antic Slab"               => "Antic Slab",
			"Anton"                    => "Anton",
			"Arapey"                   => "Arapey",
			"Arbutus"                  => "Arbutus",
			"Arbutus Slab"             => "Arbutus Slab",
			"Architects Daughter"      => "Architects Daughter",
			"Archivo Black"            => "Archivo Black",
			"Archivo Narrow"           => "Archivo Narrow",
			"Arimo"                    => "Arimo",
			"Arizonia"                 => "Arizonia",
			"Armata"                   => "Armata",
			"Artifika"                 => "Artifika",
			"Arvo"                     => "Arvo",
			"Asap"                     => "Asap",
			"Asset"                    => "Asset",
			"Astloch"                  => "Astloch",
			"Asul"                     => "Asul",
			"Atomic Age"               => "Atomic Age",
			"Aubrey"                   => "Aubrey",
			"Audiowide"                => "Audiowide",
			"Autour One"               => "Autour One",
			"Average"                  => "Average",
			"Average Sans"             => "Average Sans",
			"Averia Gruesa Libre"      => "Averia Gruesa Libre",
			"Averia Libre"             => "Averia Libre",
			"Averia Sans Libre"        => "Averia Sans Libre",
			"Averia Serif Libre"       => "Averia Serif Libre",
			"Bad Script"               => "Bad Script",
			"Balthazar"                => "Balthazar",
			"Bangers"                  => "Bangers",
			"Basic"                    => "Basic",
			"Battambang"               => "Battambang",
			"Baumans"                  => "Baumans",
			"Bayon"                    => "Bayon",
			"Belgrano"                 => "Belgrano",
			"Belleza"                  => "Belleza",
			"BenchNine"                => "BenchNine",
			"Bentham"                  => "Bentham",
			"Berkshire Swash"          => "Berkshire Swash",
			"Bevan"                    => "Bevan",
			"Bigelow Rules"            => "Bigelow Rules",
			"Bigshot One"              => "Bigshot One",
			"Bilbo"                    => "Bilbo",
			"Bilbo Swash Caps"         => "Bilbo Swash Caps",
			"Bitter"                   => "Bitter",
			"Black Ops One"            => "Black Ops One",
			"Bokor"                    => "Bokor",
			"Bonbon"                   => "Bonbon",
			"Boogaloo"                 => "Boogaloo",
			"Bowlby One"               => "Bowlby One",
			"Bowlby One SC"            => "Bowlby One SC",
			"Brawler"                  => "Brawler",
			"Bree Serif"               => "Bree Serif",
			"Bubblegum Sans"           => "Bubblegum Sans",
			"Bubbler One"              => "Bubbler One",
			"Buda"                     => "Buda",
			"Buenard"                  => "Buenard",
			"Butcherman"               => "Butcherman",
			"Butterfly Kids"           => "Butterfly Kids",
			"Cabin"                    => "Cabin",
			"Cabin Condensed"          => "Cabin Condensed",
			"Cabin Sketch"             => "Cabin Sketch",
			"Caesar Dressing"          => "Caesar Dressing",
			"Cagliostro"               => "Cagliostro",
			"Calligraffitti"           => "Calligraffitti",
			"Cambo"                    => "Cambo",
			"Candal"                   => "Candal",
			"Cantarell"                => "Cantarell",
			"Cantata One"              => "Cantata One",
			"Cantora One"              => "Cantora One",
			"Capriola"                 => "Capriola",
			"Cardo"                    => "Cardo",
			"Carme"                    => "Carme",
			"Carrois Gothic"           => "Carrois Gothic",
			"Carrois Gothic SC"        => "Carrois Gothic SC",
			"Carter One"               => "Carter One",
			"Caudex"                   => "Caudex",
			"Cedarville Cursive"       => "Cedarville Cursive",
			"Ceviche One"              => "Ceviche One",
			"Changa One"               => "Changa One",
			"Chango"                   => "Chango",
			"Chau Philomene One"       => "Chau Philomene One",
			"Chela One"                => "Chela One",
			"Chelsea Market"           => "Chelsea Market",
			"Chenla"                   => "Chenla",
			"Cherry Cream Soda"        => "Cherry Cream Soda",
			"Cherry Swash"             => "Cherry Swash",
			"Chewy"                    => "Chewy",
			"Chicle"                   => "Chicle",
			"Chivo"                    => "Chivo",
			"Cinzel"                   => "Cinzel",
			"Cinzel Decorative"        => "Cinzel Decorative",
			"Clicker Script"           => "Clicker Script",
			"Coda"                     => "Coda",
			"Coda Caption"             => "Coda Caption",
			"Codystar"                 => "Codystar",
			"Combo"                    => "Combo",
			"Comfortaa"                => "Comfortaa",
			"Coming Soon"              => "Coming Soon",
			"Concert One"              => "Concert One",
			"Condiment"                => "Condiment",
			"Content"                  => "Content",
			"Contrail One"             => "Contrail One",
			"Convergence"              => "Convergence",
			"Cookie"                   => "Cookie",
			"Copse"                    => "Copse",
			"Corben"                   => "Corben",
			"Courgette"                => "Courgette",
			"Cousine"                  => "Cousine",
			"Coustard"                 => "Coustard",
			"Covered By Your Grace"    => "Covered By Your Grace",
			"Crafty Girls"             => "Crafty Girls",
			"Creepster"                => "Creepster",
			"Crete Round"              => "Crete Round",
			"Crimson Text"             => "Crimson Text",
			"Croissant One"            => "Croissant One",
			"Crushed"                  => "Crushed",
			"Cuprum"                   => "Cuprum",
			"Cutive"                   => "Cutive",
			"Cutive Mono"              => "Cutive Mono",
			"Damion"                   => "Damion",
			"Dancing Script"           => "Dancing Script",
			"Dangrek"                  => "Dangrek",
			"Dawning of a New Day"     => "Dawning of a New Day",
			"Days One"                 => "Days One",
			"Delius"                   => "Delius",
			"Delius Swash Caps"        => "Delius Swash Caps",
			"Delius Unicase"           => "Delius Unicase",
			"Della Respira"            => "Della Respira",
			"Denk One"                 => "Denk One",
			"Devonshire"               => "Devonshire",
			"Didact Gothic"            => "Didact Gothic",
			"Diplomata"                => "Diplomata",
			"Diplomata SC"             => "Diplomata SC",
			"Domine"                   => "Domine",
			"Donegal One"              => "Donegal One",
			"Doppio One"               => "Doppio One",
			"Dorsa"                    => "Dorsa",
			"Dosis"                    => "Dosis",
			"Dr Sugiyama"              => "Dr Sugiyama",
			"Droid Sans"               => "Droid Sans",
			"Droid Sans Mono"          => "Droid Sans Mono",
			"Droid Serif"              => "Droid Serif",
			"Duru Sans"                => "Duru Sans",
			"Dynalight"                => "Dynalight",
			"EB Garamond"              => "EB Garamond",
			"Eagle Lake"               => "Eagle Lake",
			"Eater"                    => "Eater",
			"Economica"                => "Economica",
			"Electrolize"              => "Electrolize",
			"Elsie"                    => "Elsie",
			"Elsie Swash Caps"         => "Elsie Swash Caps",
			"Emblema One"              => "Emblema One",
			"Emilys Candy"             => "Emilys Candy",
			"Engagement"               => "Engagement",
			"Englebert"                => "Englebert",
			"Enriqueta"                => "Enriqueta",
			"Erica One"                => "Erica One",
			"Esteban"                  => "Esteban",
			"Euphoria Script"          => "Euphoria Script",
			"Ewert"                    => "Ewert",
			"Exo"                      => "Exo",
			"Expletus Sans"            => "Expletus Sans",
			"Fanwood Text"             => "Fanwood Text",
			"Fascinate"                => "Fascinate",
			"Fascinate Inline"         => "Fascinate Inline",
			"Faster One"               => "Faster One",
			"Fasthand"                 => "Fasthand",
			"Federant"                 => "Federant",
			"Federo"                   => "Federo",
			"Felipa"                   => "Felipa",
			"Fenix"                    => "Fenix",
			"Finger Paint"             => "Finger Paint",
			"Fjalla One"               => "Fjalla One",
			"Fjord One"                => "Fjord One",
			"Flamenco"                 => "Flamenco",
			"Flavors"                  => "Flavors",
			"Fondamento"               => "Fondamento",
			"Fontdiner Swanky"         => "Fontdiner Swanky",
			"Forum"                    => "Forum",
			"Francois One"             => "Francois One",
			"Freckle Face"             => "Freckle Face",
			"Fredericka the Great"     => "Fredericka the Great",
			"Fredoka One"              => "Fredoka One",
			"Freehand"                 => "Freehand",
			"Fresca"                   => "Fresca",
			"Frijole"                  => "Frijole",
			"Fruktur"                  => "Fruktur",
			"Fugaz One"                => "Fugaz One",
			"GFS Didot"                => "GFS Didot",
			"GFS Neohellenic"          => "GFS Neohellenic",
			"Gabriela"                 => "Gabriela",
			"Gafata"                   => "Gafata",
			"Galdeano"                 => "Galdeano",
			"Galindo"                  => "Galindo",
			"Gentium Basic"            => "Gentium Basic",
			"Gentium Book Basic"       => "Gentium Book Basic",
			"Geo"                      => "Geo",
			"Geostar"                  => "Geostar",
			"Geostar Fill"             => "Geostar Fill",
			"Germania One"             => "Germania One",
			"Gilda Display"            => "Gilda Display",
			"Give You Glory"           => "Give You Glory",
			"Glass Antiqua"            => "Glass Antiqua",
			"Glegoo"                   => "Glegoo",
			"Gloria Hallelujah"        => "Gloria Hallelujah",
			"Goblin One"               => "Goblin One",
			"Gochi Hand"               => "Gochi Hand",
			"Gorditas"                 => "Gorditas",
			"Goudy Bookletter 1911"    => "Goudy Bookletter 1911",
			"Graduate"                 => "Graduate",
			"Grand Hotel"              => "Grand Hotel",
			"Gravitas One"             => "Gravitas One",
			"Great Vibes"              => "Great Vibes",
			"Griffy"                   => "Griffy",
			"Gruppo"                   => "Gruppo",
			"Gudea"                    => "Gudea",
			"Habibi"                   => "Habibi",
			"Hammersmith One"          => "Hammersmith One",
			"Hanalei"                  => "Hanalei",
			"Hanalei Fill"             => "Hanalei Fill",
			"Handlee"                  => "Handlee",
			"Hanuman"                  => "Hanuman",
			"Happy Monkey"             => "Happy Monkey",
			"Headland One"             => "Headland One",
			"Henny Penny"              => "Henny Penny",
			"Herr Von Muellerhoff"     => "Herr Von Muellerhoff",
			"Holtwood One SC"          => "Holtwood One SC",
			"Homemade Apple"           => "Homemade Apple",
			"Homenaje"                 => "Homenaje",
			"IM Fell DW Pica"          => "IM Fell DW Pica",
			"IM Fell DW Pica SC"       => "IM Fell DW Pica SC",
			"IM Fell Double Pica"      => "IM Fell Double Pica",
			"IM Fell Double Pica SC"   => "IM Fell Double Pica SC",
			"IM Fell English"          => "IM Fell English",
			"IM Fell English SC"       => "IM Fell English SC",
			"IM Fell French Canon"     => "IM Fell French Canon",
			"IM Fell French Canon SC"  => "IM Fell French Canon SC",
			"IM Fell Great Primer"     => "IM Fell Great Primer",
			"IM Fell Great Primer SC"  => "IM Fell Great Primer SC",
			"Iceberg"                  => "Iceberg",
			"Iceland"                  => "Iceland",
			"Imprima"                  => "Imprima",
			"Inconsolata"              => "Inconsolata",
			"Inder"                    => "Inder",
			"Indie Flower"             => "Indie Flower",
			"Inika"                    => "Inika",
			"Irish Grover"             => "Irish Grover",
			"Istok Web"                => "Istok Web",
			"Italiana"                 => "Italiana",
			"Italianno"                => "Italianno",
			"Jacques Francois"         => "Jacques Francois",
			"Jacques Francois Shadow"  => "Jacques Francois Shadow",
			"Jim Nightshade"           => "Jim Nightshade",
			"Jockey One"               => "Jockey One",
			"Jolly Lodger"             => "Jolly Lodger",
			"Josefin Sans"             => "Josefin Sans",
			"Josefin Slab"             => "Josefin Slab",
			"Joti One"                 => "Joti One",
			"Judson"                   => "Judson",
			"Julee"                    => "Julee",
			"Julius Sans One"          => "Julius Sans One",
			"Junge"                    => "Junge",
			"Jura"                     => "Jura",
			"Just Another Hand"        => "Just Another Hand",
			"Just Me Again Down Here"  => "Just Me Again Down Here",
			"Kameron"                  => "Kameron",
			"Karla"                    => "Karla",
			"Kaushan Script"           => "Kaushan Script",
			"Kavoon"                   => "Kavoon",
			"Keania One"               => "Keania One",
			"Kelly Slab"               => "Kelly Slab",
			"Kenia"                    => "Kenia",
			"Khmer"                    => "Khmer",
			"Kite One"                 => "Kite One",
			"Knewave"                  => "Knewave",
			"Kotta One"                => "Kotta One",
			"Koulen"                   => "Koulen",
			"Kranky"                   => "Kranky",
			"Kreon"                    => "Kreon",
			"Kristi"                   => "Kristi",
			"Krona One"                => "Krona One",
			"La Belle Aurore"          => "La Belle Aurore",
			"Lancelot"                 => "Lancelot",
			"Lato"                     => "Lato",
			"League Script"            => "League Script",
			"Leckerli One"             => "Leckerli One",
			"Ledger"                   => "Ledger",
			"Lekton"                   => "Lekton",
			"Lemon"                    => "Lemon",
			"Libre Baskerville"        => "Libre Baskerville",
			"Life Savers"              => "Life Savers",
			"Lilita One"               => "Lilita One",
			"Limelight"                => "Limelight",
			"Linden Hill"              => "Linden Hill",
			"Lobster"                  => "Lobster",
			"Lobster Two"              => "Lobster Two",
			"Londrina Outline"         => "Londrina Outline",
			"Londrina Shadow"          => "Londrina Shadow",
			"Londrina Sketch"          => "Londrina Sketch",
			"Londrina Solid"           => "Londrina Solid",
			"Lora"                     => "Lora",
			"Love Ya Like A Sister"    => "Love Ya Like A Sister",
			"Loved by the King"        => "Loved by the King",
			"Lovers Quarrel"           => "Lovers Quarrel",
			"Luckiest Guy"             => "Luckiest Guy",
			"Lusitana"                 => "Lusitana",
			"Lustria"                  => "Lustria",
			"Macondo"                  => "Macondo",
			"Macondo Swash Caps"       => "Macondo Swash Caps",
			"Magra"                    => "Magra",
			"Maiden Orange"            => "Maiden Orange",
			"Mako"                     => "Mako",
			"Marcellus"                => "Marcellus",
			"Marcellus SC"             => "Marcellus SC",
			"Marck Script"             => "Marck Script",
			"Margarine"                => "Margarine",
			"Marko One"                => "Marko One",
			"Marmelad"                 => "Marmelad",
			"Marvel"                   => "Marvel",
			"Mate"                     => "Mate",
			"Mate SC"                  => "Mate SC",
			"Maven Pro"                => "Maven Pro",
			"McLaren"                  => "McLaren",
			"Meddon"                   => "Meddon",
			"MedievalSharp"            => "MedievalSharp",
			"Medula One"               => "Medula One",
			"Megrim"                   => "Megrim",
			"Meie Script"              => "Meie Script",
			"Merienda"                 => "Merienda",
			"Merienda One"             => "Merienda One",
			"Merriweather"             => "Merriweather",
			"Merriweather Sans"        => "Merriweather Sans",
			"Metal"                    => "Metal",
			"Metal Mania"              => "Metal Mania",
			"Metamorphous"             => "Metamorphous",
			"Metrophobic"              => "Metrophobic",
			"Michroma"                 => "Michroma",
			"Milonga"                  => "Milonga",
			"Miltonian"                => "Miltonian",
			"Miltonian Tattoo"         => "Miltonian Tattoo",
			"Miniver"                  => "Miniver",
			"Miss Fajardose"           => "Miss Fajardose",
			"Modern Antiqua"           => "Modern Antiqua",
			"Molengo"                  => "Molengo",
			"Molle"                    => "Molle",
			"Monda"                    => "Monda",
			"Monofett"                 => "Monofett",
			"Monoton"                  => "Monoton",
			"Monsieur La Doulaise"     => "Monsieur La Doulaise",
			"Montaga"                  => "Montaga",
			"Montez"                   => "Montez",
			"Montserrat"               => "Montserrat",
			"Montserrat Alternates"    => "Montserrat Alternates",
			"Montserrat Subrayada"     => "Montserrat Subrayada",
			"Moul"                     => "Moul",
			"Moulpali"                 => "Moulpali",
			"Mountains of Christmas"   => "Mountains of Christmas",
			"Mouse Memoirs"            => "Mouse Memoirs",
			"Mr Bedfort"               => "Mr Bedfort",
			"Mr Dafoe"                 => "Mr Dafoe",
			"Mr De Haviland"           => "Mr De Haviland",
			"Mrs Saint Delafield"      => "Mrs Saint Delafield",
			"Mrs Sheppards"            => "Mrs Sheppards",
			"Muli"                     => "Muli",
			"Mystery Quest"            => "Mystery Quest",
			"Neucha"                   => "Neucha",
			"Neuton"                   => "Neuton",
			"New Rocker"               => "New Rocker",
			"News Cycle"               => "News Cycle",
			"Niconne"                  => "Niconne",
			"Nixie One"                => "Nixie One",
			"Nobile"                   => "Nobile",
			"Nokora"                   => "Nokora",
			"Norican"                  => "Norican",
			"Nosifer"                  => "Nosifer",
			"Nothing You Could Do"     => "Nothing You Could Do",
			"Noticia Text"             => "Noticia Text",
			"Noto Sans"                => "Noto Sans",
			"Noto Serif"               => "Noto Serif",
			"Nova Cut"                 => "Nova Cut",
			"Nova Flat"                => "Nova Flat",
			"Nova Mono"                => "Nova Mono",
			"Nova Oval"                => "Nova Oval",
			"Nova Round"               => "Nova Round",
			"Nova Script"              => "Nova Script",
			"Nova Slim"                => "Nova Slim",
			"Nova Square"              => "Nova Square",
			"Numans"                   => "Numans",
			"Nunito"                   => "Nunito",
			"Odor Mean Chey"           => "Odor Mean Chey",
			"Offside"                  => "Offside",
			"Old Standard TT"          => "Old Standard TT",
			"Oldenburg"                => "Oldenburg",
			"Oleo Script"              => "Oleo Script",
			"Oleo Script Swash Caps"   => "Oleo Script Swash Caps",
			"Open Sans"                => "Open Sans",
			"Open Sans Condensed"      => "Open Sans Condensed",
			"Oranienbaum"              => "Oranienbaum",
			"Orbitron"                 => "Orbitron",
			"Oregano"                  => "Oregano",
			"Orienta"                  => "Orienta",
			"Original Surfer"          => "Original Surfer",
			"Oswald"                   => "Oswald",
			"Over the Rainbow"         => "Over the Rainbow",
			"Overlock"                 => "Overlock",
			"Overlock SC"              => "Overlock SC",
			"Ovo"                      => "Ovo",
			"Oxygen"                   => "Oxygen",
			"Oxygen Mono"              => "Oxygen Mono",
			"PT Mono"                  => "PT Mono",
			"PT Sans"                  => "PT Sans",
			"PT Sans Caption"          => "PT Sans Caption",
			"PT Sans Narrow"           => "PT Sans Narrow",
			"PT Serif"                 => "PT Serif",
			"PT Serif Caption"         => "PT Serif Caption",
			"Pacifico"                 => "Pacifico",
			"Paprika"                  => "Paprika",
			"Parisienne"               => "Parisienne",
			"Passero One"              => "Passero One",
			"Passion One"              => "Passion One",
			"Patrick Hand"             => "Patrick Hand",
			"Patrick Hand SC"          => "Patrick Hand SC",
			"Patua One"                => "Patua One",
			"Paytone One"              => "Paytone One",
			"Peralta"                  => "Peralta",
			"Permanent Marker"         => "Permanent Marker",
			"Petit Formal Script"      => "Petit Formal Script",
			"Petrona"                  => "Petrona",
			"Philosopher"              => "Philosopher",
			"Piedra"                   => "Piedra",
			"Pinyon Script"            => "Pinyon Script",
			"Pirata One"               => "Pirata One",
			"Plaster"                  => "Plaster",
			"Play"                     => "Play",
			"Playball"                 => "Playball",
			"Playfair Display"         => "Playfair Display",
			"Playfair Display SC"      => "Playfair Display SC",
			"Podkova"                  => "Podkova",
			"Poiret One"               => "Poiret One",
			"Poller One"               => "Poller One",
			"Poly"                     => "Poly",
			"Pompiere"                 => "Pompiere",
			"Pontano Sans"             => "Pontano Sans",
			"Port Lligat Sans"         => "Port Lligat Sans",
			"Port Lligat Slab"         => "Port Lligat Slab",
			"Prata"                    => "Prata",
			"Preahvihear"              => "Preahvihear",
			"Press Start 2P"           => "Press Start 2P",
			"Princess Sofia"           => "Princess Sofia",
			"Prociono"                 => "Prociono",
			"Prosto One"               => "Prosto One",
			"Puritan"                  => "Puritan",
			"Purple Purse"             => "Purple Purse",
			"Quando"                   => "Quando",
			"Quantico"                 => "Quantico",
			"Quattrocento"             => "Quattrocento",
			"Quattrocento Sans"        => "Quattrocento Sans",
			"Questrial"                => "Questrial",
			"Quicksand"                => "Quicksand",
			"Quintessential"           => "Quintessential",
			"Qwigley"                  => "Qwigley",
			"Racing Sans One"          => "Racing Sans One",
			"Radley"                   => "Radley",
			"Raleway"                  => "Raleway",
			"Raleway Dots"             => "Raleway Dots",
			"Rambla"                   => "Rambla",
			"Rammetto One"             => "Rammetto One",
			"Ranchers"                 => "Ranchers",
			"Rancho"                   => "Rancho",
			"Rationale"                => "Rationale",
			"Redressed"                => "Redressed",
			"Reenie Beanie"            => "Reenie Beanie",
			"Revalia"                  => "Revalia",
			"Ribeye"                   => "Ribeye",
			"Ribeye Marrow"            => "Ribeye Marrow",
			"Righteous"                => "Righteous",
			"Risque"                   => "Risque",
			"Roboto"                   => "Roboto",
			"Roboto Condensed"         => "Roboto Condensed",
			"Roboto Slab"              => "Roboto Slab",
			"Rochester"                => "Rochester",
			"Rock Salt"                => "Rock Salt",
			"Rokkitt"                  => "Rokkitt",
			"Romanesco"                => "Romanesco",
			"Ropa Sans"                => "Ropa Sans",
			"Rosario"                  => "Rosario",
			"Rosarivo"                 => "Rosarivo",
			"Rouge Script"             => "Rouge Script",
			"Ruda"                     => "Ruda",
			"Rufina"                   => "Rufina",
			"Ruge Boogie"              => "Ruge Boogie",
			"Ruluko"                   => "Ruluko",
			"Rum Raisin"               => "Rum Raisin",
			"Ruslan Display"           => "Ruslan Display",
			"Russo One"                => "Russo One",
			"Ruthie"                   => "Ruthie",
			"Rye"                      => "Rye",
			"Sacramento"               => "Sacramento",
			"Sail"                     => "Sail",
			"Salsa"                    => "Salsa",
			"Sanchez"                  => "Sanchez",
			"Sancreek"                 => "Sancreek",
			"Sansita One"              => "Sansita One",
			"Sarina"                   => "Sarina",
			"Satisfy"                  => "Satisfy",
			"Scada"                    => "Scada",
			"Schoolbell"               => "Schoolbell",
			"Seaweed Script"           => "Seaweed Script",
			"Sevillana"                => "Sevillana",
			"Seymour One"              => "Seymour One",
			"Shadows Into Light"       => "Shadows Into Light",
			"Shadows Into Light Two"   => "Shadows Into Light Two",
			"Shanti"                   => "Shanti",
			"Share"                    => "Share",
			"Share Tech"               => "Share Tech",
			"Share Tech Mono"          => "Share Tech Mono",
			"Shojumaru"                => "Shojumaru",
			"Short Stack"              => "Short Stack",
			"Siemreap"                 => "Siemreap",
			"Sigmar One"               => "Sigmar One",
			"Signika"                  => "Signika",
			"Signika Negative"         => "Signika Negative",
			"Simonetta"                => "Simonetta",
			"Sintony"                  => "Sintony",
			"Sirin Stencil"            => "Sirin Stencil",
			"Six Caps"                 => "Six Caps",
			"Skranji"                  => "Skranji",
			"Slackey"                  => "Slackey",
			"Smokum"                   => "Smokum",
			"Smythe"                   => "Smythe",
			"Sniglet"                  => "Sniglet",
			"Snippet"                  => "Snippet",
			"Snowburst One"            => "Snowburst One",
			"Sofadi One"               => "Sofadi One",
			"Sofia"                    => "Sofia",
			"Sonsie One"               => "Sonsie One",
			"Sorts Mill Goudy"         => "Sorts Mill Goudy",
			"Source Code Pro"          => "Source Code Pro",
			"Source Sans Pro"          => "Source Sans Pro",
			"Special Elite"            => "Special Elite",
			"Spicy Rice"               => "Spicy Rice",
			"Spinnaker"                => "Spinnaker",
			"Spirax"                   => "Spirax",
			"Squada One"               => "Squada One",
			"Stalemate"                => "Stalemate",
			"Stalinist One"            => "Stalinist One",
			"Stardos Stencil"          => "Stardos Stencil",
			"Stint Ultra Condensed"    => "Stint Ultra Condensed",
			"Stint Ultra Expanded"     => "Stint Ultra Expanded",
			"Stoke"                    => "Stoke",
			"Strait"                   => "Strait",
			"Sue Ellen Francisco"      => "Sue Ellen Francisco",
			"Sunshiney"                => "Sunshiney",
			"Supermercado One"         => "Supermercado One",
			"Suwannaphum"              => "Suwannaphum",
			"Swanky and Moo Moo"       => "Swanky and Moo Moo",
			"Syncopate"                => "Syncopate",
			"Tangerine"                => "Tangerine",
			"Taprom"                   => "Taprom",
			"Tauri"                    => "Tauri",
			"Telex"                    => "Telex",
			"Tenor Sans"               => "Tenor Sans",
			"Text Me One"              => "Text Me One",
			"The Girl Next Door"       => "The Girl Next Door",
			"Tienne"                   => "Tienne",
			"Tinos"                    => "Tinos",
			"Titan One"                => "Titan One",
			"Titillium Web"            => "Titillium Web",
			"Trade Winds"              => "Trade Winds",
			"Trocchi"                  => "Trocchi",
			"Trochut"                  => "Trochut",
			"Trykker"                  => "Trykker",
			"Tulpen One"               => "Tulpen One",
			"Ubuntu"                   => "Ubuntu",
			"Ubuntu Condensed"         => "Ubuntu Condensed",
			"Ubuntu Mono"              => "Ubuntu Mono",
			"Ultra"                    => "Ultra",
			"Uncial Antiqua"           => "Uncial Antiqua",
			"Underdog"                 => "Underdog",
			"Unica One"                => "Unica One",
			"UnifrakturCook"           => "UnifrakturCook",
			"UnifrakturMaguntia"       => "UnifrakturMaguntia",
			"Unkempt"                  => "Unkempt",
			"Unlock"                   => "Unlock",
			"Unna"                     => "Unna",
			"VT323"                    => "VT323",
			"Vampiro One"              => "Vampiro One",
			"Varela"                   => "Varela",
			"Varela Round"             => "Varela Round",
			"Vast Shadow"              => "Vast Shadow",
			"Vibur"                    => "Vibur",
			"Vidaloka"                 => "Vidaloka",
			"Viga"                     => "Viga",
			"Voces"                    => "Voces",
			"Volkhov"                  => "Volkhov",
			"Vollkorn"                 => "Vollkorn",
			"Voltaire"                 => "Voltaire",
			"Waiting for the Sunrise"  => "Waiting for the Sunrise",
			"Wallpoet"                 => "Wallpoet",
			"Walter Turncoat"          => "Walter Turncoat",
			"Warnes"                   => "Warnes",
			"Wellfleet"                => "Wellfleet",
			"Wendy One"                => "Wendy One",
			"Wire One"                 => "Wire One",
			"Yanone Kaffeesatz"        => "Yanone Kaffeesatz",
			"Yellowtail"               => "Yellowtail",
			"Yeseva One"               => "Yeseva One",
			"Yesteryear"               => "Yesteryear",
			"Zeyada"                   => "Zeyada"
		);


		/*-----------------------------------------------------------------------------------*/
		/* The Options Array */
		/*-----------------------------------------------------------------------------------*/

// Set the Options Array
		global $of_options;
		$of_options = array();

		$of_options[] = array( "name" => "General Settings",
							   "type" => "heading",
			'label'	=> 'aaaaaaaaa'
		);

		$logo         = get_template_directory_uri( 'template_directory' ) . '/images/logo.png';
		$favicon      = get_template_directory_uri( 'template_directory' ) . '/images/favicon.ico';
		$of_options[] = array( "name" => "Header Logo",
							   "desc" => "Enter URL or Upload an image file as your logo.",
							   "id"   => "site_logo",
							   "std"  => $logo,
							   "type" => "media"
		);
		$of_options[] = array( "name" => "Sticky Header Logo ",
							   "desc" => "Enter URL or Upload an image file as your sticky header logo. max-size(Xpx x 50px)",
							   "id"   => "sticky_logo",
							   "std"  => $logo,
							   "type" => "media"
		);
		$of_options[] = array( "name" => "Logo Column Width (1->12) ",
							   "id"   => "width_column_logo",
							   "type" => "text",
							   "std"  => "3",
							   "desc" => "",
		);

		$of_options[] = array( "name" => "Favicon",
							   "desc" => "Enter URL or upload an icon image to represent your website's favicon (16px x 16px)",
							   "id"   => "favicon",
							   "std"  => $favicon,
							   "type" => "media"
		);

		$of_options[] = array( "name" => "Display Settings",
							   "type" => "heading",
							   "icon" => ADMIN_IMAGES . "ico-view.png"
		);
		$url          = get_template_directory_uri( 'template_directory' ) . '/inc/admin/layout/';
		$of_options[] = array( "name" => "Archive Display Settings",
							   "desc" => "",
							   "id"   => "archiver",
							   "std"  => "<h3>Archive Display Settings</h3>",
							   "icon" => true,
							   "type" => "info" );
		$of_options[] = array( "name"    => "Archiver Layout",
							   "desc"    => "Select layout for Archive Page.",
							   "id"      => "layout",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png',
								   '3c-fixed'   => $url . 'body-left-right.png',
								   '3c-r-fixed' => $url . 'body-cr.jpg'
							   )
		);
		$of_options[] = array( "name" => "Hide Breadcrumbs?",
							   "id"   => "opt_hide_breadcrumbs_archiver",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide Breadcrumbs"
		);
		$of_options[] = array( "name" => "Hide Title",
							   "id"   => "opt_hide_title_archiver",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide title"
		);
		$of_options[] = array( "name" => "Custom Heading Background",
							   "id"   => "custom_header_background_archiver",
							   "type" => "media",
							   "desc" => "Enter URL or upload an image to set as your heading background"

		);
		$of_options[] = array( "name" => "Height Custom Heading ",
							   "desc" => "Use a number without 'px', default is 100. ex: 100",
							   "id"   => "height_custom_heading_archiver",
							   "std"  => "100",
							   "type" => "text" );

		$of_options[] = array( "name" => "Text color for custom heading",
							   "id"   => "text_color_archiver",
							   "std"  => "#fff",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Excerpt Length",
							   "desc" => "Enter the number of words you want to cut from the content to be the excerpt of search and archive and portfolio page.",
							   "id"   => "excerpt_length_blog",
							   "std"  => "20",
							   "type" => "text" );
		$of_options[] = array( "name" => "Show category",
							   "desc" => "show/hidden",
							   "id"   => "show_category",
							   "std"  => 0,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Show date",
							   "desc" => "show/hidden",
							   "id"   => "show_date",
							   "std"  => 1,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Show Author",
							   "desc" => "show/hidden",
							   "id"   => "show_author",
							   "std"  => 1,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Show Comment ",
							   "desc" => "show/hidden",
							   "id"   => "show_comment",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Date Format",
							   "desc" => "<a href='http://codex.wordpress.org/Formatting_Date_and_Time'>Formatting Date and Time</a>",
							   "id"   => "date_format",
							   "std"  => "F j, Y",
							   "type" => "text" );
		//Post displays
		$of_options[] = array( "name" => "Post Display Settings",
							   "desc" => "",
							   "id"   => "display_settings",
							   "std"  => "<h3>Post Display Settings</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name"    => "Select Layout Default",
							   "desc"    => "Select layout for Post.",
							   "id"      => "post_page_layout",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png'
							   )
		);

		$of_options[] = array( "name" => "Hide Breadcrumbs?",
							   "id"   => "opt_hide_breadcrumbs",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide Breadcrumbs"
		);
		$of_options[] = array( "name" => "Hide Title",
							   "id"   => "opt_hide_title",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide title"
		);
		$of_options[] = array( "name" => "Custom Heading Background",
							   "id"   => "custom_header_background",
							   "type" => "media",
							   "desc" => "Enter URL or upload an image to set as your heading background"

		);
		$of_options[] = array( "name" => "Height Custom Heading ",
							   "desc" => "Use a number without 'px', default is 100. ex: 100",
							   "id"   => "height_custom_heading",
							   "std"  => "100",
							   "type" => "text" );

		$of_options[] = array( "name" => "Text color for custom heading",
							   "id"   => "text_color",
							   "std"  => "#fff",
							   "type" => "color"
		);
		//Page displays
		$of_options[] = array( "name" => "Page Display Settings",
							   "desc" => "",
							   "id"   => "page_display_settings",
							   "std"  => "<h3>Page Display Settings</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name"    => "Select Layout Default",
							   "desc"    => "Select layout for Page.",
							   "id"      => "page_layout",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png'
							   )
		);

		$of_options[] = array( "name" => "Hide Breadcrumbs?",
							   "id"   => "opt_page_hide_breadcrumbs",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide Breadcrumbs"
		);
		$of_options[] = array( "name" => "Hide Title",
							   "id"   => "opt_page_hide_title",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide title"
		);
		$of_options[] = array( "name" => "Custom Heading Background",
							   "id"   => "page_custom_header_background",
							   "type" => "media",
							   "desc" => "Enter URL or upload an image to set as your heading background"

		);
		$of_options[] = array( "name" => "Height Custom Heading ",
							   "desc" => "Use a number without 'px', default is 100. ex: 100",
							   "id"   => "page_height_custom_heading",
							   "std"  => "100",
							   "type" => "text" );

		$of_options[] = array( "name" => "Text color for custom heading",
							   "id"   => "page_text_color",
							   "std"  => "#fff",
							   "type" => "color"
		);
		//Front page displays
		$of_options[] = array( "name" => "Front page displays",
							   "desc" => "",
							   "id"   => "front_page",
							   "std"  => "<h3>Front page displays settings: Posts page</h3>",
							   "icon" => true,
							   "type" => "info" );
		$of_options[] = array( "name"    => "Select Style",
							   "id"      => "style_front_page",
							   "std"     => "basic",
							   "type"    => "select",
							   "options" => array(
								   'basic'             => 'Basic',
								   'grid_two_column'   => 'Grid Two Column',
								   'grid_three_column' => 'Grid Three Column',
								   'grid_four_column'  => 'Grid Four Column',
								   'timeline'          => 'Timeline'
							   ),
		);
		$of_options[] = array( "name"    => "Layout",
							   "desc"    => "Select layout for front page.",
							   "id"      => "layout_front_page",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png',
								   '3c-fixed'   => $url . 'body-left-right.png',
								   '3c-r-fixed' => $url . 'body-cr.jpg'
							   )
		);

		$of_options[] = array( "name" => "Custoom Title",
							   "id"   => "custom_title_front_page",
							   "type" => "text",
							   "std"  => "Home",

		);
		$of_options[] = array( "name" => "Custom Heading Background",
							   "id"   => "custom_header_background_front_page",
							   "type" => "media",
							   "desc" => "Enter URL or upload an image to set as your heading background"

		);
		$of_options[] = array( "name" => "Height Custom Heading ",
							   "desc" => "Use a number without 'px', default is 100. ex: 100",
							   "id"   => "height_custom_heading_front_page",
							   "std"  => "100",
							   "type" => "text" );

		$of_options[] = array( "name" => "Text color for custom heading",
							   "id"   => "text_color_front_page",
							   "std"  => "#fff",
							   "type" => "color"
		);

		//General Settings
		$of_options[] = array( "name" => "Header Options",
							   "type" => "heading"
		);
		$of_options[] = array( "name" => "Header Info",
							   "desc" => "",
							   "id"   => "header_info",
							   "std"  => "<h3>Header Layout Options</h3>",
							   "icon" => true,
							   "type" => "info" );
		$of_options[] = array( "name"    => "Select a Header Layout",
							   "desc"    => "",
							   "id"      => "header_layout",
							   "std"     => "header_v1",
							   "type"    => "images",
							   "options" => array(
								   "header_v1" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header1.jpg",
								   "header_v2" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header2.jpg",
								   "header_v3" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header3.jpg",
								   "header_v4" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header4.jpg",
								   "header_v5" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header5.jpg",
								   "header_v6" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header6.jpg",
								   "header_v7" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header7.jpg",
								   "header_v8" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/header8.jpg",
							   ) );
		$of_options[] = array( "name" => "Header Menu Information",
							   "desc" => "",
							   "id"   => "menu_info",
							   "std"  => "<h3>Header Menu Options</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name" => "Sticky Header on scroll",
							   "desc" => "Check this to make header stick to top on scroll",
							   "id"   => "header_sticky",
							   "std"  => 0,
							   "type" => "checkbox"
		);


		$of_options[] = array( "name" => "Sticky Header height",
							   "desc" => "Set height of sticky header in px.",
							   "id"   => "header_height_sticky",
							   "std"  => "153",
							   "min"  => "30",
							   "step" => "1",
							   "max"  => "650",
							   "type" => "sliderui"
		);
		$of_options[] = array( "name" => "Sticky Header Border Bottom Color",
							   "id"   => "border_color-sticky",
							   "std"  => "#ddd",
							   "type" => "color"
		);

		$of_options[] = array( "name" => "Header Background color",
							   "desc" => "Pick a background color for header",
							   "id"   => "bg_header_color",
							   "std"  => "#fff",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Background Opacity",
							   "id"   => "bg_opacity",
							   "std"  => "100",
							   "min"  => "0",
							   "step" => "1",
							   "max"  => "100",
							   "type" => "sliderui"
		);
		$of_options[] = array( "name" => "Header Text color",
							   "desc" => "Pick a text color for header",
							   "id"   => "menu_text_color",
							   "std"  => "#333",
							   "type" => "color"
		);
		$of_options[] = array( "name"    => "Main Menu Font Size",
							   "desc"    => "Default is 13",
							   "id"      => "font_size_main_menu",
							   "std"     => "13",
							   "type"    => "select",
							   "options" => $font_sizes
		);
		$of_options[] = array( "name"    => "Main Menu Font Weight",
							   "desc"    => "Default bold",
							   "id"      => "font_weight_main_menu",
							   "std"     => "600",
							   "type"    => "select",
							   "options" => array( 'bold' => 'Bold', 'normal' => 'Normal', '100' => '100', '200' => '200', '300' => '300', '400' => '400', '500' => '500', '600' => '600', '700' => '700', '800' => '800', '900' => '900' ),
		);

		$of_options[] = array( "name" => "Using Menu One Page",
							   "desc" => "Checked if you setup menu for onepage site",
							   "id"   => "one_page_main_menu",
							   "std"  => 0,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Enable Mega Menu",
							   "desc" => "Enable/Desable",
							   "id"   => "enable_mega_menu",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Top Drawer Options",
							   "desc" => "",
							   "id"   => "drawer_options",
							   "std"  => "<h3>Top Drawer Options</h3>",
							   "icon" => true,
							   "type" => "info" );
		$of_options[] = array( "name" => "Show or Hide Top Drawer",
							   "desc" => "show/hide",
							   "id"   => "show_drawer",
							   "std"  => 0,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Top Drawer Background color",
							   "desc" => "Pick a background color",
							   "id"   => "bg_drawer_color",
							   "std"  => "#EB583C",
							   "type" => "color" );

		$of_options[] = array( "name" => "Top Drawer Text color",
							   "desc" => "Pick a text color for Top Drawer",
							   "id"   => "drawer_text_color",
							   "std"  => "#fff",
							   "type" => "color"
		);
		$of_options[] = array( "name"    => "Top Drawer Columns",
							   "desc"    => "",
							   "id"      => "drawer_columns",
							   "std"     => "1",
							   "options" => array( '1' => '1', '2' => '2', '3' => '3', '4' => '4' ),
							   "type"    => "select" );

		$of_options[] = array( "name"    => "Top Drawer Style",
							   "desc"    => "",
							   "id"      => "drawer_style",
							   "std"     => "style1",
							   "type"    => "images",
							   "options" => array(
								   "style1" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/drawer_1.jpg",
								   "style2" => get_template_directory_uri( 'template_directory' ) . "/images/patterns/drawer_2.jpg",
							   ) );

		$of_options[] = array( "name" => "Show/Hide Drawer",
							   "id"   => "drawer_open",
							   "desc" => "show/hide",
							   "std"  => "#fff",
							   "std"  => "0",
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => " Right Drawer Options",
							   "desc" => "",
							   "id"   => "drawer_right",
							   "std"  => "<h3> Right Drawer Options</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name" => "Show or Hide Right Drawer",
							   "desc" => "show/hide",
							   "id"   => "show_drawer_right",
							   "std"  => 0,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Right Drawer Background color",
							   "desc" => "Pick a background color",
							   "id"   => "bg_drawer_right_color",
							   "std"  => "#181818",
							   "type" => "color" );

		$of_options[] = array( "name" => "Right Drawer Text color",
							   "desc" => "Pick a text color for Right Drawer",
							   "id"   => "drawer_right_text_color",
							   "std"  => "#a9a9a9",
							   "type" => "color"
		);

		$of_options[] = array( "name" => "Right Drawer Icon",
							   "id"   => "icon_drawer_right",
							   "type" => "text",
							   "std"  => "fa-bars",
							   "desc" => "Enter <a href=\"http://fontawesome.io/icons/\" target=\"_blank\" >FontAwesome</a> icon name. For example: fa-bars, fa-user"
		);
		$of_options[] = array( "name" => "Top Header Options",
							   "desc" => "",
							   "id"   => "top_header_info",
							   "std"  => "<h3>Top Header </h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name" => "Show or Hide Top Bar",
							   "desc" => "show/hide",
							   "id"   => "topbar_show",
							   "std"  => 1,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name"    => "Top Header Font Size",
							   "desc"    => "Default is 12",
							   "id"      => "font_size_top_header",
							   "std"     => "12",
							   "type"    => "select",
							   "options" => $font_sizes
		);
		$of_options[] = array( "name" => "Top Header Background color",
							   "desc" => "Pick a background color",
							   "id"   => "bg_top_color",
							   "std"  => "#fff",
							   "type" => "color" );

		$of_options[] = array( "name" => "Top Header Border color",
							   "desc" => "Pick a  color",
							   "id"   => "border_top_color",
							   "std"  => "#EEE",
							   "type" => "color" );

		$of_options[] = array( "name" => "Top Header Text color",
							   "desc" => "Pick a text color for Top Header",
							   "id"   => "top_header_text_color",
							   "std"  => "#CCCCCC",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Top Header Link color",
							   "desc" => "Pick a link color for Top Header",
							   "id"   => "top_header_link_color",
							   "std"  => "#333",
							   "type" => "color"
		);

		$of_options[] = array( "name" => "Top Left Sidebar Width (1->12)",
							   "id"   => "width_left_top_sidebar",
							   "type" => "text",
							   "std"  => "6",
							   "desc" => "",
		);

//		$of_options[] = array( "name" => "Show or Hide input search on menu of canvas",
//							   "desc" => "show/hide",
//							   "id"   => "show_search",
//							   "std"  => 1,
//							   "type" => "checkbox"
//		);

		$of_options[] = array( "name" => "Footer Options",
							   "type" => "heading"
		);

		$of_options[] = array( "name"    => "Footer Columns",
							   "desc"    => "",
							   "id"      => "footer_widgets_columns",
							   "std"     => "4",
							   "options" => array( '1' => '1', '2' => '2', '3' => '3', '4' => '4' ),
							   "type"    => "select" );

		$of_options[] = array( "name" => "Footer text color ",
							   "desc" => "Pick a text color for Footer",
							   "id"   => "text_footer_color",
							   "std"  => "#BABABA",
							   "type" => "color" );

		$of_options[] = array( "name" => "Footer heading text color",
							   "desc" => "Pick a text color for Header Footer",
							   "id"   => "text_footer_header_color",
							   "std"  => "#fff",
							   "type" => "color" );

		$of_options[] = array( "name" => "Footer background color",
							   "desc" => "Pick a background color.",
							   "id"   => "bg_footer_color",
							   "std"  => "#222",
							   "type" => "color" );


		$of_options[] = array( "name" => "Copyright background color",
							   "desc" => "Pick a background color.",
							   "id"   => "bg_coppyright_color",
							   "std"  => "#333",
							   "type" => "color" );

		$of_options[] = array( "name" => "Copyright Text Color",
							   "desc" => "Pick a text color for Footer",
							   "id"   => "text_coppyright_color",
							   "std"  => "#B9B9B9",
							   "type" => "color" );

		$of_options[] = array( "name" => "Copyright Text",
							   "desc" => "Enter copyright text",
							   "id"   => "footer_text",
							   "std"  => "&copy; 2014 MONTANA &#8226; Built by obTheme.<br/>Premium WordPress Themes by obTheme.",
							   "type" => "textarea"
		);

		$of_options[] = array( "name" => "Tracking Code",
							   "desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
							   "id"   => "google_analytics",
							   "std"  => "",
							   "type" => "textarea"
		);
		$of_options[] = array( "name" => "Show Back To Top",
							   "desc" => "show/hidden",
							   "id"   => "totop_show",
							   "std"  => 1,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Styling Options",
							   "type" => "heading"
		);

		$of_options[] = array( "name" => "Body Info",
							   "desc" => "",
							   "id"   => "body_info",
							   "std"  => "<h3>Body Options</h3>",
							   "icon" => true,
							   "type" => "info"
		);

		$of_options[] = array( "name" => "Disable responsive",
							   "desc" => "Disable",
							   "id"   => "disable_responsive",
							   "std"  => 0,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name" => "Show preload",
							   "desc" => "show/hide",
							   "id"   => "show_perload",
							   "std"  => 1,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name"    => "Layout",
							   "desc"    => "Select a layout",
							   "id"      => "box_layout",
							   "std"     => "wide",
							   "type"    => "select",
							   "options" => array(
								   'boxed' => 'Boxed',
								   'wide'  => 'Wide',
							   ) );
		$of_options[] = array( "name" => "Body Background Color",
							   "desc" => "Pick a background color for the theme (default: #000).",
							   "id"   => "body_background",
							   "std"  => "#fff",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Background Pattern",
							   "desc" => "Check the box to display a pattern in the background. If checked, select the pattern from below.",
							   "id"   => "user_bg_pattern",
							   "std"  => 0,
							   "type" => "checkbox"
		);
		$of_options[] = array( "name"    => "Background Patterns",
							   "desc"    => "",
							   "id"      => "bg_pattern",
							   "type"    => "images",
							   "options" => array(
								   get_template_directory_uri() . "/images/patterns/pattern1.png"  => get_template_directory_uri() . "/images/patterns/pattern1.png",
								   get_template_directory_uri() . "/images/patterns/pattern2.png"  => get_template_directory_uri() . "/images/patterns/pattern2.png",
								   get_template_directory_uri() . "/images/patterns/pattern3.png"  => get_template_directory_uri() . "/images/patterns/pattern3.png",
								   get_template_directory_uri() . "/images/patterns/pattern4.png"  => get_template_directory_uri() . "/images/patterns/pattern4.png",
								   get_template_directory_uri() . "/images/patterns/pattern5.png"  => get_template_directory_uri() . "/images/patterns/pattern5.png",
								   get_template_directory_uri() . "/images/patterns/pattern6.png"  => get_template_directory_uri() . "/images/patterns/pattern6.png",
								   get_template_directory_uri() . "/images/patterns/pattern7.png"  => get_template_directory_uri() . "/images/patterns/pattern7.png",
								   get_template_directory_uri() . "/images/patterns/pattern8.png"  => get_template_directory_uri() . "/images/patterns/pattern8.png",
								   get_template_directory_uri() . "/images/patterns/pattern9.png"  => get_template_directory_uri() . "/images/patterns/pattern9.png",
								   get_template_directory_uri() . "/images/patterns/pattern10.png" => get_template_directory_uri() . "/images/patterns/pattern10.png",
							   ) );
		//$background_img = get_template_directory_uri( 'template_directory' ) . '/images/bg.jpg';
		$of_options[] = array( "name" => "Upload Background",
							   "desc" => "Upload your own background",
							   "id"   => "bg_pattern_upload",
			// "std"  => $background_img,
							   "type" => "media"
		);
		$of_options[] = array( "name"    => "Background Repeat",
							   "desc"    => "",
							   "id"      => "bg_repeat",
							   "std"     => "no-repeat",
							   "type"    => "select",
							   "options" => array( 'repeat' => 'repeat', 'repeat-x' => 'repeat-x', 'repeat-y' => 'repeat-y', 'no-repeat' => 'no-repeat' )
		);
		$of_options[] = array( "name"    => "Background Position",
							   "desc"    => "Set position for your background",
							   "id"      => "bg_position",
							   "std"     => "center center",
							   "type"    => "select",
							   "options" => array( 'left top'      => 'Left Top',
												   'left center'   => 'Left Center',
												   'left bottom'   => 'Left Bottom',
												   'right top'     => 'Right Top',
												   'right center'  => 'Right Center',
												   'right bottom'  => 'Right Bottom',
												   'center top'    => 'Center Top',
												   'center center' => 'Center Center',
												   'center bottom' => 'Center Bottom'
							   )
		);
		$of_options[] = array( "name"    => "Background Attachment",
							   "desc"    => "Select the type of background attachment",
							   "id"      => "bg_attachment",
							   "std"     => "inherit",
							   "type"    => "select",
							   "options" => array( 'scroll'  => 'scroll',
												   'fixed'   => 'fixed',
												   'local'   => 'local',
												   'initial' => 'initial',
												   'inherit' => 'inherit'
							   )
		);

		$of_options[] = array( "name" => "Body Text Color",
							   "desc" => "Pick a text color for the theme.",
							   "id"   => "body_color",
							   "std"  => "#5a5a5a",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Body Link Color",
							   "desc" => "Pick a link color for the theme.",
							   "id"   => "body_link_color",
							   "std"  => "#fe4444",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "Border Color",
							   "desc" => "",
							   "id"   => "border_body_color",
							   "std"  => "#ccc",
							   "type" => "color"
		);
		$of_options[] = array( "name" => "RTL Support",
							   "desc" => "",
							   "id"   => "rtl_support_header",
							   "std"  => "<h3>RTL Support</h3>",
							   "icon" => true,
							   "type" => "info"
		);
		$of_options[] = array( "name" => "",
							   "desc" => "Enable/Desable",
							   "id"   => "rtl_support",
							   "std"  => 0,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Social Sharing Box",
							   "type" => "heading"
		);
		$of_options[] = array( "name" => "Text",
							   "id"   => "text_social",
							   "type" => "text",
							   "desc" => "Insert text you want to appear in social sharing box of your post"

		);
		$of_options[] = array( "name" => "Facebook",
							   "desc" => "Show the facebook sharing option in blog posts.",
							   "id"   => "sharing_facebook",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Twitter",
							   "desc" => "Show the twitter sharing option in blog posts.",
							   "id"   => "sharing_twitter",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "LinkedIn",
							   "desc" => "Show the LinkIn sharing option in blog posts.",
							   "id"   => "sharing_linkedin",
							   "std"  => 1,
							   "type" => "checkbox"
		);


		$of_options[] = array( "name" => "Tumblr",
							   "desc" => "Show the Tumblr sharing option in blog posts.",
							   "id"   => "sharing_tumblr",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Google Plus",
							   "desc" => "Show the g+ sharing option in blog posts.",
							   "id"   => "sharing_google",
							   "std"  => 1,
							   "type" => "checkbox"
		);

		$of_options[] = array( "name" => "Pinterest",
							   "desc" => "Show the pinterest sharing option in blog posts.",
							   "id"   => "sharing_pinterest",
							   "std"  => 1,
							   "type" => "checkbox"
		);

// Backup WooCommerce
		$of_options[] = array( "name" => "WooCommerce",
							   "type" => "heading",
							   "icon" => ADMIN_IMAGES . "woo_icon.png"
		);

//		$of_options[] = array( "name"    => "Category Product Layout",
//							   "desc"    => "",
//							   "id"      => "category_product_layout",
//							   "std"     => "grid",
//							   "options" => array( 'grid' => __('Grid','montana'),'list' => __('List','montana'),'both'=>__('Show Both','montana') ),
//							   "type"    => "select"
//		);
		$of_options[] = array( "name" => "Number of Products per Page",
							   "desc" => "Insert the number of posts to display per page.",
							   "id"   => "number_per_page",
							   "std"  => "12",
							   "type" => "text"
		);

		$of_options[] = array( "name" => "Animation Product Options",
							   "desc" => "",
							   "id"   => "animation_option",
							   "std"  => "<h3>Animation Product Options</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[]          = array( "name"    => "Product Hover",
										"desc"    => "",
										"id"      => "product_image_hover",
										"std"     => "4",
										"options" => array( 'changeimages' => 'Change Images', 'flip_back' => 'Flip Back' ),
										"type"    => "select"
		);
		$of_options[]          = array( "name"    => "CSS Animation",
										"desc"    => "",
										"id"      => "product_css_animation",
										"options" => array( '' => __( "No", "montana" ), "animated" => __( "ScaleIn Effect", "montana" ) ),
										"desc"    => __( "Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "montana" ),
										"type"    => "select"
		);
		$wc_heading_background = get_template_directory_uri( 'template_directory' ) . '/images/bg.jpg';
		$of_options[]          = array( "name" => "Heading Background",
										"desc" => "Please choose an image file for heading background.",
										"id"   => "wc_heading_background",
										"std"  => $wc_heading_background,
										"type" => "media"
		);
		$of_options[]          = array( "name"    => "Shop and Category Shop Layout",
										"desc"    => "Select layout for Product Page.",
										"id"      => "product_layout",
										"std"     => "2c-l-fixed",
										"type"    => "images",
										"options" => array(
											'1col-fixed' => $url . 'body-full.png',
											'2c-l-fixed' => $url . 'sidebar-left.png',
											'2c-r-fixed' => $url . 'sidebar-right.png',
										)
		);

		$of_options[] = array( "name"    => "Single Product Layout",
							   "desc"    => "Select layout for Single Product.",
							   "id"      => "product_layout_single",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png',
							   )
		);

		$of_options[] = array( "name"    => "Shop Archive Default View",
							   "id"      => "shop_archive_default_view",
							   "options" => array( 'grid' => __( "Grid", "montana" ), "list" => __( "List", "montana" ) ),
							   "desc"    => __( "Set default view list product for shop archive page(Grid or List)", "montana" ),
							   "type"    => "select"
		);

// Portfolio
		$of_options[] = array( "name" => "Portfolio",
							   "type" => "heading",
							   "icon" => ADMIN_IMAGES . "icon-portfolio.png"
		);
		$of_options[] = array( "name" => "Number of Items per Page",
							   "desc" => "Insert the number of item to display per page",
							   "id"   => "n_per_page",
							   "std"  => "6",
							   "type" => "text"
		);
		$of_options[] = array( "name"    => "Images Hover Effects",
							   "desc"    => "",
							   "id"      => "portfolio_effects",
							   "std"     => "effect-layla",
							   "options" => array( 'effect-layla' => 'Layla',
												   'effect-bubba' => 'Bubba',
												   'effect-romeo' => 'Romeo',
												   'effect-oscar' => 'Oscar',
												   'effects_zoom' => 'Zoom In'
							   ),
							   "type"    => "select"
		);
		$of_options[] = array( "name"    => "Select Layout Default",
							   "desc"    => "Select layout for Portfolio.",
							   "id"      => "portfolio_layout",
							   "std"     => "2c-l-fixed",
							   "type"    => "images",
							   "options" => array(
								   '1col-fixed' => $url . 'body-full.png',
								   '2c-l-fixed' => $url . 'sidebar-left.png',
								   '2c-r-fixed' => $url . 'sidebar-right.png'
							   )
		);
		$of_options[] = array( "name"    => "Column",
							   "desc"    => "",
							   "id"      => "portfolio_column",
							   "std"     => "three-col",
							   "options" => array( 'two-col'   => '2 Column',
												   'three-col' => '3 Column',
												   'four-col'  => '4 Column',
							   ),
							   "type"    => "select"
		);
		$of_options[] = array( "name" => "Hide Breadcrumbs?",
							   "id"   => "opt_hide_breadcrumbs_portfolio",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide Breadcrumbs"
		);
		$of_options[] = array( "name" => "Hide Title",
							   "id"   => "opt_hide_title_portfolio",
							   "std"  => 0,
							   "type" => "checkbox",
							   "desc" => "Check this box to hide/unhide title"
		);
		$of_options[] = array( "name" => "Custom Heading Background",
							   "id"   => "custom_header_background_portfolio",
							   "type" => "media",
							   "desc" => "Enter URL or upload an image to set as your heading background"

		);
		$of_options[] = array( "name" => "Height Custom Heading ",
							   "desc" => "Use a number without 'px', default is 100. ex: 100",
							   "id"   => "height_custom_heading_portfolio",
							   "std"  => "100",
							   "type" => "text" );

		$of_options[] = array( "name" => "Text color for custom heading",
							   "id"   => "text_color_portfolio",
							   "std"  => "#fff",
							   "type" => "color"
		);
// typofraphy
		$of_options[] = array( "name" => "Typography",
							   "type" => "heading"
		);

		$of_options[]   = array( "name" => "body Info",
								 "desc" => "",
								 "id"   => "body_info",
								 "std"  => "<h3>Body Options</h3>",
								 "icon" => true,
								 "type" => "info" );
		$standard_fonts = array(
			'0'                                                    => 'Select Font',
			'Arial, Helvetica, sans-serif'                         => 'Arial, Helvetica, sans-serif',
			"'Arial Black', Gadget, sans-serif"                    => "'Arial Black', Gadget, sans-serif",
			"'Bookman Old Style', serif"                           => "'Bookman Old Style', serif",
			"'Comic Sans MS', cursive"                             => "'Comic Sans MS', cursive",
			"Courier, monospace"                                   => "Courier, monospace",
			"Garamond, serif"                                      => "Garamond, serif",
			"Georgia, serif"                                       => "Georgia, serif",
			"Impact, Charcoal, sans-serif"                         => "Impact, Charcoal, sans-serif",
			"'Lucida Console', Monaco, monospace"                  => "'Lucida Console', Monaco, monospace",
			"'Lucida Sans Unicode', 'Lucida Grande', sans-serif"   => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
			"'MS Sans Serif', Geneva, sans-serif"                  => "'MS Sans Serif', Geneva, sans-serif",
			"'MS Serif', 'New York', sans-serif"                   => "'MS Serif', 'New York', sans-serif",
			"'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
			"Tahoma, Geneva, sans-serif"                           => "Tahoma, Geneva, sans-serif",
			"'Times New Roman', Times, serif"                      => "'Times New Roman', Times, serif",
			"'Trebuchet MS', Helvetica, sans-serif"                => "'Trebuchet MS', Helvetica, sans-serif",
			"Verdana, Geneva, sans-serif"                          => "Verdana, Geneva, sans-serif"
		);
		$of_options[]   = array( "name"    => "Select Standards Fonts Family",
								 "desc"    => "Select a font family for body text",
								 "id"      => "standard_body",
								 "std"     => "",
								 "type"    => "select",
								 "options" => $standard_fonts );

		$of_options[] = array( "name"    => "Select Google Fonts Family",
							   "desc"    => "Select a font family for body text",
							   "id"      => "google_body_font",
							   "std"     => "Raleway",
							   "type"    => "select",
							   "options" => $google_fonts
		);

		$of_options[] = array( "name"    => "Body Font Size (px)",
							   "desc"    => "Default is 14",
							   "id"      => "body_font_size",
							   "std"     => "14",
							   "type"    => "select",
							   "options" => $font_sizes
		);
		$of_options[] = array( "name"    => "Font Weight",
							   "desc"    => "",
							   "id"      => "font_weight_body",
							   "std"     => "500",
							   "type"    => "select",
							   "options" => $font_weight
		);

		$of_options[] = array( "name" => "Headings Info",
							   "desc" => "",
							   "id"   => "headings_info",
							   "std"  => "<h3>Headings Options</h3>",
							   "icon" => true,
							   "type" => "info" );

		$of_options[] = array( "name"    => "Select Standards Fonts Family",
							   "desc"    => "Select a font family for body text",
							   "id"      => "standard_heading",
							   "std"     => "",
							   "type"    => "select",
							   "options" => $standard_fonts );

		$of_options[] = array( "name"    => "Select Google Font",
							   "desc"    => "Select a font family for headings",
							   "id"      => "google_headings",
							   "std"     => "Raleway",
							   "type"    => "select",
							   "options" => $google_fonts
		);
		$of_options[] = array( "name" => "Fonts Color",
							   "desc" => "Pick a text color for the Headings.",
							   "id"   => "headings_color",
							   "std"  => "#3f3f3f",
							   "type" => "color"
		);
		$of_options[] = array( "name"  => "Font H1",
							   "desc"  => "",
							   "id"    => "font_h1",
							   "class" => "ds_font",
							   "std"   => "Font H1",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h1",
							   "std"     => "26",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H1",
							   "desc"    => "",
							   "id"      => "font_weight_h1",
							   "std"     => "600",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h1",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);

		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h1",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);

		$of_options[] = array( "name"  => "Font H2",
							   "desc"  => "",
							   "id"    => "font_h2",
							   "class" => "ds_font",
							   "std"   => "Font H2",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h2",
							   "std"     => "24",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H2",
							   "desc"    => "",
							   "id"      => "font_weight_h2",
							   "std"     => "600",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h2",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);
		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h2",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);

		$of_options[] = array( "name"  => "Font H3",
							   "desc"  => "",
							   "id"    => "font_h3",
							   "class" => "ds_font",
							   "std"   => "Font H3",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h3",
							   "std"     => "22",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H3",
							   "desc"    => "",
							   "id"      => "font_weight_h3",
							   "std"     => "600",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h3",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);
		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h3",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);

		$of_options[] = array( "name"  => "Font H4",
							   "desc"  => "",
							   "id"    => "font_h4",
							   "class" => "ds_font",
							   "std"   => "Font H4",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h4",
							   "std"     => "18",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H4",
							   "desc"    => "",
							   "id"      => "font_weight_h4",
							   "std"     => "600",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h4",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);
		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h4",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);
		$of_options[] = array( "name"  => "Font H5",
							   "desc"  => "",
							   "id"    => "font_h5",
							   "class" => "ds_font",
							   "std"   => "Font H5",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h5",
							   "std"     => "14",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H5",
							   "desc"    => "",
							   "id"      => "font_weight_h5",
							   "std"     => "600",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h5",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);
		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h5",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);
		$of_options[] = array( "name"  => "Font H6",
							   "desc"  => "",
							   "id"    => "font_h6",
							   "class" => "ds_font",
							   "std"   => "Font H6",
							   "icon"  => true,
							   "type"  => "info" );

		$of_options[] = array( "name"    => "Font Size (px)",
							   "desc"    => "",
							   "id"      => "font_size_h6",
							   "std"     => "12",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_sizes
		);

		$of_options[] = array( "name"    => "Font Weight H6",
							   "desc"    => "",
							   "id"      => "font_weight_h6",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_weight
		);
		$of_options[] = array( "name"    => "Font Style",
							   "desc"    => "",
							   "id"      => "font_style_h6",
							   "std"     => "normal",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $font_style
		);
		$of_options[] = array( "name"    => "Text Transform",
							   "desc"    => "",
							   "id"      => "text_transform_h6",
							   "std"     => "none",
							   "type"    => "select",
							   "class"   => "select25",
							   "options" => $text_transform
		);
		$of_options[] = array( "name" => "Bootstrap Options",
							   "type" => "heading",
							   "icon" => ADMIN_IMAGES . "icon-bootstrap.png"
		);

//$of_options[] = array( 	"name" 		=> "jQuery",
//                        "desc" 		=> "",
//                        "id" 		=> "jquery_iclu",
//                        "std" 		=> "jquery_cdn",
//                        "type" 		=> "radio",
//                        "options"       =>array('jquery_cdn'=>'Include From CDN','jquery_resource'=>'Include From Resource')
//                    );
		$of_options[] = array( "name"    => "Bootstrap jQuery",
							   "desc"    => "",
							   "id"      => "bootstrap_js",
							   "std"     => "js_cdn",
							   "type"    => "radio",
							   "options" => array( 'js_cdn'      => 'Include From CDN',
												   'js_resource' => 'Include From Resource'
							   )
		);
		$of_options[] = array( "name"    => "Bootstrap CSS",
							   "desc"    => "",
							   "id"      => "bootstrap_css",
							   "std"     => "css_cdn",
							   "type"    => "radio",
							   "options" => array( 'css_cdn'      => 'Include From CDN',
												   'css_resource' => 'Include From Resource'
							   )
		);
		$of_options[] = array( "name"    => "Font Awesome",
							   "desc"    => "",
							   "id"      => "font_awesome",
							   "std"     => "font_awesome_cdn",
							   "type"    => "radio",
							   "options" => array( 'font_awesome_cdn'      => 'Include From CDN',
												   'font_awesome_resource' => 'Include From Resource'
							   )
		);
// css custom
		$of_options[] = array( "name" => "Custom CSS",
							   "type" => "heading"
		);
		$of_options[] = array( "name" => "Custom CSS",
							   "desc" => "",
							   "id"   => "css_custom",
							   "std"  => ".class_custom{}",
							   "type" => "textarea"
		);

		$of_options[] = array( "name" => "Backup Options",
							   "type" => "heading",
							   "icon" => ADMIN_IMAGES . "icon-slider.png"
		);

		$of_options[] = array( "name" => "Backup and Restore Options",
							   "id"   => "of_backup",
							   "std"  => "",
							   "type" => "backup",
							   "desc" => 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
		);

		$of_options[] = array( "name" => "Transfer Theme Options Data",
							   "id"   => "of_transfer",
							   "std"  => "",
							   "type" => "transfer",
							   "desc" => 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".',
		);
	}
	//End function: of_options()
}//End chack if function exists: of_options()
?>
