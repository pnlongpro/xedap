<?php

function obtheme_makesite() {
	if ( isset( $_REQUEST['makesite'] ) && current_user_can( 'manage_options' ) ) {
		require_once OB_INC . 'import' . DIRECTORY_SEPARATOR . 'ob_import.php';
		die;
	}
}

?>
