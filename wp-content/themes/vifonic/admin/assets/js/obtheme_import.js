/**
 * File: OBtheme import
 * Description: Action import data demo, demo files to make site as demo site
 * Author: Andy Ha (tu@wpbriz.com)
 * Copyright 2007-2014 wpbriz.com. All rights reserved.
 */

/**
 * Function import
 * Call ajax to process
 * @constructor
 */

function OBtheme_import(type) {
	import_type('woo_setting', type);
	jQuery(document).ready(function () {
		jQuery(".obimport_core .meter > span").animate({ width: '20px' });
	});
}
function import_type(type, method) {
	jQuery(document).ready(function () {
		jQuery(".ob_process_bar").slideDown('fast');
		jQuery.ajax({
			type   : 'POST',
			data   : 'makesite=true&action=obtheme_makesite&method=' + method + '&type=' + type,
			url    : ajaxurl,
			success: function (html) {
				if (html == 'done') {
					jQuery(".obimport_widgets .meter > span").animate({ width: '345px' }, 'slow');
					location.reload();
				}
				else if ((html == 'setting') || (html == 'menus') || (html == 'slider') || (html == 'widgets') || (html == 'core')) {
					switch (html) {
						case 'menus':
							jQuery(".obimport_setting .meter > span").animate({ width: '345px' }, 'slow');
							break;
						case 'slider':
							jQuery(".obimport_menus .meter > span").animate({ width: '345px' }, 'slow');
							break;
						case 'widgets':
							jQuery(".obimport_slider .meter > span").animate({ width: '345px' }, 'slow');
							break;
						case 'core':
							//console.log();
							jQuery(".obimport_core .meter > span").animate({ width: parseInt(jQuery('.obimport_core .meter > span').width()) + 1 + 'px' }, 'slow');

							break;
						default :
							jQuery(".obimport_core .meter > span").animate({ width: '345px' }, 'slow');
					}
					import_type(html, method);
				}
				else {
					alert(html);
					console.log(html);
				}
			},
			error  : function (html) {
				alert(html);
				console.log(html);
			}
		})
		;
	})
	;
}
/**
 * Function remove demo data
 * @constructor
 */
function OBtheme_remove() {

}
