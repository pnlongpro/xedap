<?php
//Template Name: Home Page
get_header();
?>

<?php
if( 1 ) :
	?>
	<div class="main-slider">
		<?php echo do_shortcode('[rev_slider top-slide]') ?>
	</div>
<?php endif; ?>
	<div class="container">
	<div id="primary" class="content-area">
		<div class="top-icon-box">
			<ul class="list-inline clearfix">
				<li class="best row-1" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-chat-luong-tot-nhat.png" alt="Thiết kế web chuyên nghiệp - chất lượng tốt nhất">
					<h4>CHẤT LƯỢNG TỐT NHẤT</h4>
					<p>Nắm rõ yêu cầu khách hàng. Quy trình triển khai tiêu chuẩn, Phân tích thiết kế web kỹ lưỡng. Áp dụng công nghệ tiên tiến nhất</p>
				</li>
				<li class="difference row-1" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-khac-biet.png" alt="Thiết kế web chuyên nghiệp - khác biệt">
					<h4>Khác biệt</h4>
					<p>Cam kết thiết kế website dành riêng cho Quý khách, khác biệt và độc đáo</p>
				</li>
				<li class="support row-2" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-bao-hanh-tron-doi.png" alt="Thiết kế web chuyên nghiệp - bảo hành trọn đời">
					<h4>BẢO HÀNH TRỌN ĐỜI</h4>
					<p>Hỗ trợ khách hàng 24/7 từ lúc tạo dựng sản phẩm đến quá trình vận hành. Bảo hành website trọn đời</p>
				</li>
				<li class="simple row-2" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-don-gian-toi-uu.png" alt="Thiết kế web chuyên nghiệp - đơn giản - tối ưu">
					<h4>ĐƠN GIẢN &amp; TỐI ƯU</h4>
					<p>Đơn giản nhưng tinh tế. Đơn giản nhưng ấn tượng. Đơn giản nhưng hiệu quả. Đơn giản để tối ưu</p>
				</li>
				<li class="full-stack row-2" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-giai-phap-tron-goi.png" alt="Thiết kế web chuyên nghiệp - giải pháp toàn diện">
					<h4>GIẢI PHÁP TOÀN DIỆN</h4>
					<p>Vifonic cung cấp cho Quý khách giải pháp toàn diện từ việc thiết kế website chuyên nghiệp đến chiến lược marketing hiệu quả nhất</p>
				</li>
				<li class="everything row-1" style="position: relative; top: 0px; opacity: 1;">
					<img src="http://vifonic.com/my-content/themes/pitado/assets/img/feature/thiet-ke-web-chuyen-nghiep-moi-thu-ban-muon.png" alt="Thiết kế web chuyên nghiệp - khác biệt">
					<h4>Mọi thứ bạn muốn</h4>
					<p>Vifonic đáp ứng mọi yêu cầu của Quý khách trong quá trình thiết kế web, từ giao diện, chức năng đến tính Bảo mật</p>
				</li>
			</ul>
		</div>
		<main id="main" class="site-main" role="main">
			<div class="list-home-product">
				<?php
					$list_panel = get_field('product_panel');
					if( $list_panel ) :
						foreach( $list_panel as $panel_item ) :
						if( $panel_item['select_type'] == 'product' ){
							$class = 'type-product-list';
							?>
							<div class="list-home-item <?php echo $class; ?>">
								<div class="entry-title">
									<h2><?php echo $panel_item['title']; ?></h2>
								</div>
								<div class="row">

								<?php
									$loop = $panel_item['select_product'];

								foreach( $loop as $post ) : setup_postdata($post);
									echo '<div class="col-md-3 col-sm-4 col-xs-12">';
										get_template_part('template-parts/content','product');
									echo '</div>';
								endforeach; wp_reset_postdata();
								?>
								</div>
							</div>
							<?php
						}else{
							$class = 'type-category-list';

							$args = array(
								'post_type'			=> 'product',
								'posts_per_page'	=> 8,
								'tax_query'			=> array(
									array(
										'taxonomy'			=> 'product_cat',
										'field'				=> 'slug',
										'terms'				=> $panel_item['select_category']->slug
									)
								)
							);
							query_posts($args);
							if( have_posts() ) :
							?>
						<div class="list-home-item <?php echo $class; ?>">
							<div class="entry-title">
								<h2><a href="<?php echo get_term_link($panel_item['select_category']->slug,
										'product_cat')
									?>"><?php echo $panel_item['select_category']->name;
										?></a></h2>
							</div>
							<div class="row">
								<?php
									while( have_posts() ) : the_post();
										echo '<div class="col-md-3 col-sm-4 col-xs-12">';
										get_template_part('template-parts/content','product');
										echo '</div>';
									endwhile; wp_reset_postdata();
								?>
							</div>
						</div>
					<?php
						endif;
					}
				?>
				<?php
					endforeach;
					endif;//end list_panel
				?>
			</div>
		</main>
		<?php
			$list_partner = get_field('partner');
			if( $list_partner ) :
		?>
		<div class="partner">
			<div class="list_partner">
				<?php
					foreach( $list_partner as $value ){
						$html = '';
						$html .= '<div class="partner-item">';
						if( $value['link'] ){
							$html .= '<a href="'.$value['link'].'">';
							$html .= '<img src="'.$value['logo'].'" alt="" />';
							$html .= '</a>';
						}else{
							$html .= '<img src="'.$value['logo'].'" alt="" />';
						}
						$html .= '</div>';
						echo $html;
					}
				?>
			</div>
		</div>
		<?php
			endif;
		?>
	</div>
<?php
get_footer();
?>