<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/9/2015
 * Time: 2:17 PM
 */
//Template Name: Đặt hàng
get_header(); ?>

	<div id="primary" class="content-area">

		<div class="row">
			<main id="main" class="site-main col-md-6 col-sm-12 col-md-push-3 col-sm-push-0" role="main">
				<div class="top-site-main">
					<div class="breadcrumbs-wrapper">
						<?php vifonic_breadcrumbs(); ?>
					</div>
				</div>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->
				<?php
					if( isset( $_GET['productID'] ) && $_GET['productID'] != '' ){
						$post = get_post($_GET['productID']);
						echo '<h3 class="product-title bg-info">Sản phẩm: '. get_the_title($post->ID) .'</h3>';
					}
				?>
				<?php

				if( isset($_POST['checkout'] ) ){
					$error = array();
					if( ! isset( $_POST['txt-name'] ) || $_POST['txt-name'] =='' ){
						$error[] = 'Tên không được bỏ trông';
					}
					if( ! isset( $_POST['txt-phone'] ) || $_POST['txt-phone'] =='' ){
						$error[] = 'Số điện thoại không được bỏ trống';
					}
					$html ='';
					$html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
					$html .= '<html lang="en">';
					$html .= '<body>';
					$html .= '<p><strong>Họ Tên:</strong> '. $_POST['txt-name'] .'';
					$html .= '<p><strong>Email:</strong> '. $_POST['txt-email'] .'';
					$html .= '<p><strong>Số điện thoại:</strong> '. $_POST['txt-phone'] .'';
					$html .= '<p><strong>Địa chỉ:</strong> '. $_POST['txt-address'] .'';
					$html .= '<p><strong>Nội dung: </strong> '. $_POST['txt-messages'] .'';
					$html .= '<p><strong>Sản phẩm:</strong> '. get_the_title($post->ID) .'';
					$html .= '<p><strong>Ngày đặt hàng:</strong> '. date("j - n - Y") .'';
					$html .= '</body>';
					$html .= '</html>';
					$subject = '[Đặt hàng] - '. get_the_title($post->ID) .' - '. $_POST['txt-name'].'';
					//send mail
//					$to = get_option( 'admin_email' );;
					$to = 'pnlongpro@gmai.com';
					$message = $html;
					//var_dump($message); exit;
					$headers ='';
					$headers .= 'From: Đông Đô <dongdohospital@gmail.com>' . "\r\n";
					$headers .= 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$attachments = '';
					if( isset($error) && $error != '' ){
						echo '<p style="padding: 15px;" class="bg-danger">';
						foreach( $error as $value ){
							echo $value .'<br />';
						}
						echo '</p>';
					}else{
						$sendmail = wp_mail( $to, $subject, $message, $headers, $attachments );
						if( $sendmail ){
							echo '<p style="padding: 15px;" class="bg-success">Đặt hàng thành công.</p>';
						}else{
							echo '<p style="padding: 15px;" class="bg-danger">Có lỗi, vui lòng thử lại</p>';
						}
					}

				}

				?>
				<form action="" method="post" >

					<div class="form-group">
						<input class="form-control" type="text" name="txt-name" placeholder="Họ và tên (bắt buộc)" />
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="txt-phone" placeholder="Số điện thoại (bắt buộc)" />
					</div>
					<div class="form-group">
						<input class="form-control" type="email" name="txt-email" placeholder="Email" />
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="txt-address" placeholder="Địa chỉ" />
					</div>
					<div class="form-group">
						<textarea class="form-control" name="txt-messages" id="" cols="30" rows="5" placeholder="Nội dung"></textarea>
					</div>
					<input type="hidden" value="<?php echo get_the_title($post->ID) ?>" name="txt-product" />
					<div class="text-right">
						<input class="btn btn-primary btn-lg" type="submit" name="checkout" value="Đặt hàng" />
					</div>
				</form>
			</main><!-- #main -->
			<?php get_sidebar() ?>
			<?php get_sidebar( 'second' ) ?>
		</div>
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>