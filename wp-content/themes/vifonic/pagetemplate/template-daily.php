<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/10/2015
 * Time: 10:44 AM
 */
//Template name: Đại Lý


get_header(); ?>

<div id="primary" class="content-area">
	<div class="row">
		<main id="main" class="site-main col-md-6 col-sm-12 col-md-push-3 col-sm-push-0" role="main">
			<div class="top-site-main">
				<div class="breadcrumbs-wrapper">
					<?php vifonic_breadcrumbs(); ?>
				</div>
			</div>
			<div class="list-page">
				<?php
					$list = get_field('agency-list');
					if( $list ){
						foreach( $list as $value ){
							$post = $value['select_agency'];
							setup_postdata($post)
						?>
							<div class="agency-item">
								<div class="row">
									<div class="col-md-4 ">
										<?php
											$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
											$params = array('width' => 300,'height' => 340);
											$img = bfi_thumb($image_attributes[0], $params);
										?>
										<div class="thumb"><a href="<?php echo get_permalink($post->ID) ?>"><img src="<?php echo $img ?>" alt="" /></a></div>

									</div>
									<div class="col-md-8 entry-content">
										<h3><a href="<?php echo get_permalink($post->ID) ?>"><?php echo get_the_title($post->ID) ?></a></h3>
										<div class="description">
											<p>
												<?php echo $value['short_description'] ?>
											</p>
										</div>
									</div>
								</div>
							</div>
						<?php
						}
					}
				?>
			</div>
		</main><!-- #main -->
		<?php get_sidebar() ?>
		<?php get_sidebar( 'second' ) ?>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
