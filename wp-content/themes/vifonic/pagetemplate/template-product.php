<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/9/2015
 * Time: 4:21 PM
 */
//Template Name: Sản phẩm
get_header(); ?>

<div id="primary" class="content-area">
	<div class="row">
		<main id="main" class="site-main col-md-6 col-sm-12 col-md-push-3 col-sm-push-0" role="main">
			<div class="top-site-main">
				<div class="breadcrumbs-wrapper">
					<?php vifonic_breadcrumbs(); ?>
				</div>
			</div>
			<div class="list-product clearfix">
				<?php
					$args = array(
						'post_type'			=> 'product',
						'posts_per_page'	=> -1
					);
				query_posts($args);
				?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'product' ); ?>
				<?php endwhile; // End of the loop.
				wp_reset_postdata();
				// ?>
			</div>
		</main><!-- #main -->
		<?php get_sidebar() ?>
		<?php get_sidebar( 'second' ) ?>
	</div>
</div><!-- #primary -->

<?php get_footer(); ?>
