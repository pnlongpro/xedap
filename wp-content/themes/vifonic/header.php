<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vifonic
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href=" <?php echo ot_get_option( 'favicon' ) ?>" type="image/x-icon" />
	<meta property="fb:app_id" content="" />
	<meta property="fb:admins" content="" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<nav class="top-bar">
			<div class="container">
				<div class="pull-left">
					<span>Hotline: <a href="tel:0946229911">0946.22.99.11</a></span>
					<span>Email: <a href="mailto:contact@vifonic.com">contact@vifonic.com</a></span>
				</div>
				<div class="pull-right">
					<ul class="search">
						<li class="li-search-form">
							<?php get_search_form( true ) ?>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="top-header">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 header-left">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php echo ot_get_option( 'logo' ) ?>" alt="<?php bloginfo( 'name' ); ?>" />
						</a>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 header-right">
						<img src="http://vifonic.com/my-content/themes/pitado/assets/img/top-right-banner.jpg" alt="Thiết kế web chuyên nghiệp trọn gói Vifonic">
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default menu">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse main-navigation" id="bs-example-navbar-collapse-1">
					<div class="container">
					<ul id="menu-left-top-menu" class="nav navbar-nav">
						<?php
						if ( has_nav_menu( 'primary' ) ) {
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'container'      => false,
								'items_wrap'     => '%3$s'
							) );
						} else {
							wp_nav_menu( array(
								'theme_location' => '',
								'container'      => false,
								'items_wrap'     => '%3$s'
							) );
						}
						?>
					</ul>
					<ul id="menu-right-top-menu" class="nav navbar-nav navbar-right">
						<?php
						if ( has_nav_menu( 'second_menu' ) ) {
							wp_nav_menu( array(
								'theme_location' => 'second_menu',
								'container'      => false,
								'items_wrap'     => '%3$s'
							) );
						} else {
							wp_nav_menu( array(
								'theme_location' => '',
								'container'      => false,
								'items_wrap'     => '%3$s'
							) );
						}
						?>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
		</nav>
	</header>
	<!-- #masthead -->

	<section id="content" class="site-content">

