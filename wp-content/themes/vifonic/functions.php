<?php
/**
 * vifonic functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package vifonic
 */

if ( ! function_exists( 'vifonic_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vifonic_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on vifonic, use a find and replace
	 * to change 'vifonic' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'vifonic', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'blog_thumbnail', 280, 360, true );
	add_image_size( 'woocommerce_thumbnail', 300, 410, true );
	register_nav_menus( array(
		'primary'     => esc_html__( 'Menu Left', 'vifonic' ),
		'second_menu'     => esc_html__( 'Menu right', 'vifonic' ),
		'footer_menu' => esc_html__( 'Footer Menu', 'vifonic' ),
	) );


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'vifonic_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // vifonic_setup
add_action( 'after_setup_theme', 'vifonic_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vifonic_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vifonic_content_width', 640 );
}
add_action( 'after_setup_theme', 'vifonic_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vifonic_widgets_init() {
//	register_sidebar( array(
//		'name'          => esc_html__( 'Sidebar', 'vifonic' ),
//		'id'            => 'sidebar-1',
//		'description'   => '',
//		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
//		'after_widget'  => '</aside>',
//		'before_title'  => '<h2 class="widget-title">',
//		'after_title'   => '</h2>',
//	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Footer', 'vifonic' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar left', 'vifonic' ),
		'id'            => 'sidebar-3',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Right', 'vifonic' ),
		'id'            => 'sidebar-4',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'vifonic_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
require get_template_directory() .'/rslib/rslib.php';
function vifonic_scripts() {



	//Bootstrap

	wp_deregister_style( 'vifonic-bootstrap-style' );
	wp_register_style( 'vifonic-bootstrap-style', get_template_directory_uri() .'/css/bootstrap/css/bootstrap.min.css', array(), true );
	wp_enqueue_style( 'vifonic-bootstrap-style' );
	//bootstrap-script
	wp_deregister_script( 'vifonic-bootrap-script' );
	wp_register_script( 'vifonic-bootrap-script', get_template_directory_uri() . '/css/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'vifonic-bootrap-script' );
	//font-awesome

	wp_deregister_style( 'vifonic-font-awesome' );
	wp_register_style( 'vifonic-font-awesome', get_template_directory_uri() .'/css/font-awesome/css/font-awesome.min.css', array(), true );
	wp_enqueue_style( 'vifonic-font-awesome' );
	/*
	 * ----------------------------------owl carousel--------------------------------------
	 */

	wp_deregister_style( 'vifonic-owl-style' );
	wp_register_style( 'vifonic-owl-style', get_template_directory_uri() .'/js/owl-carousel/owl.carousel.css', array(), true );

	wp_deregister_script( 'vifonic-owl-script' );
	wp_register_script( 'vifonic-owl-script', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array( 'jquery' ), '', true );

	//------------------------------------------------------------------------------


	/*
	 * ----------------------------------flexslider--------------------------------------
	 */
	wp_deregister_style( 'vifonic-flexslider-style' );
	wp_register_style( 'vifonic-flexslider-style', get_template_directory_uri() .'/js/flexslider/flexslider.css', array
	(), true );

	wp_deregister_script( 'vifonic-flexslider-script' );
	wp_register_script( 'vifonic-flexslider-script', get_template_directory_uri() . '/js/flexslider/jquery
	.flexslider-min.js', array(	'jquery' ), '', true );

	wp_deregister_script( 'vifonic-retina-script' );
	wp_register_script( 'vifonic-retina-script', get_template_directory_uri() . '/js/jquery.retina.min.js', array( 'jquery' ), '', true );

	wp_deregister_script( 'vifonic-stick-script' );
	wp_register_script( 'vifonic-stick-script', get_template_directory_uri() . '/js/jquery.sticky.js', array(
		'jquery' ), '', true );
	wp_enqueue_script( 'vifonic-stick-script' );

	//------------------------------------------------------------------------------
//    Menu mobile

	wp_deregister_script( 'vifonic-menumobile' );
	wp_register_script( 'vifonic-menumobile', get_template_directory_uri() . '/js/menu-mobile/jquery.slicknav.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'vifonic-menumobile' );

	//style
	wp_deregister_style( 'vifonic-menumobile-style' );
	wp_register_style( 'vifonic-menumobile-style', get_template_directory_uri() .'/js/menu-mobile/slicknav.min.css', array(), true );
	wp_enqueue_style( 'vifonic-menumobile-style' );



//------------------------------------------------------------------------------''


	if( is_front_page() ){
		wp_enqueue_script( 'vifonic-owl-script' );
		wp_enqueue_style( 'vifonic-owl-style' );
	}
	if( is_singular('product') ){
		wp_enqueue_script( 'vifonic-flexslider-script' );
		wp_enqueue_script( 'vifonic-retina-script' );
		wp_enqueue_style( 'vifonic-flexslider-style' );
	}
	/*
	 * ----------------------------------end owl carousel--------------------------------------
	 */

	wp_enqueue_script( 'vifonic-custom-script', get_template_directory_uri() . '/js/custom-script.js', array(), '20120206', true );

	wp_enqueue_style( 'vifonic-style', get_stylesheet_uri() );
	wp_enqueue_style( 'vifonic-style-main', get_template_directory_uri() . '/css/style.css',array(),true );
	wp_enqueue_style( 'vifonic-responsive', get_template_directory_uri() . '/css/responsive.css',array(),true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vifonic_scripts' );

/**
 * Implement the admin index file
 */
//require get_template_directory() . '/admin/index.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/custom-function.php';
require get_template_directory() .'/inc/extras/BFI_Thumb.php';
require get_template_directory() .'/inc/add-product-post-type.php';
require get_template_directory() .'/inc/extra-class-widget.php';
require get_template_directory() .'/inc/widget/widget.php';
require get_template_directory() .'/inc/add-shiping.php';

/******************************************************************/

//add_filter( 'comment_form_defaults',	'change_comment_form_defaults');
//function change_comment_form_defaults($default) {
//	$commenter = wp_get_current_commenter();
//	$default['fields']['email'] .= '<p class="comment-form-author">' .
//		'<label for="city">'. __('City') . '</label>
//						<span class="required">*</span>
//		        <input id="city" name="city" size="30" type="text" /></p>';
//
//	return $default;
//}
//add_filter( 'preprocess_comment', 'verify_comment_meta_data' );
//function verify_comment_meta_data($commentdata) {
//	if ( ! isset( $_POST['city'] ) )
//		wp_die( __('Error: please fill the required field (city).') );
//	return $commentdata;
//}
//add_action( 'comment_post',	'save_comment_meta_data' );
//function save_comment_meta_data( $comment_id ) {
//	add_comment_meta( $comment_id, 'city', $_POST['city'] );
//}
//add_filter( 'get_comment_author_link',	'attach_city_to_author' );
//function attach_city_to_author( $author ) {
//	$city = get_comment_meta( get_comment_ID(), 'city', true );
//	if ( $city )
//		$author .= " ($city)";
//	return $author;
//}


//function remove_comment_fields($fields) {
//	//unset($fields['email']);
//	unset($fields['url']);
//	return $fields;
//}
//add_filter('comment_form_default_fields', 'remove_comment_fields');

function get_term_parent($id, $taxonomy){
	$term = get_term($id, $taxonomy);
	$termParent = $term->parent;
	return $termParent;
}

if( !function_exists('table_detail') ){
	function table_detail( $table = '' ){
			echo '<div class="table-responsive">';
			echo '<table class="table table-striped table-detail">';
			if ( $table['header'] ) {
				echo '<thead>';
				echo '<tr>';
				foreach ( $table['header'] as $th ) {
					echo '<th>';
					echo $th['c'];
					echo '</th>';
				}
				echo '</tr>';
				echo '</thead>';
			}
			echo '<tbody>';
			foreach ( $table['body'] as $tr ) {
				echo '<tr>';
				foreach ( $tr as $td ) {
					echo '<td width="50%">';
					echo $td['c'];
					echo '</td>';
				}
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
			echo '</div>';
	}
}



if ( !function_exists( 'filter_search' ) ) {
	function filter_search( $search_args ) {

		/* taxonomy query and meta query arrays */
		$tax_query  = array();
		$meta_query = array();

		/* property status taxonomy query */
		if ( ( !empty( $_GET['category'] ) ) ) {
			$tax_query[] = array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $_GET['category']
			);
		}
		/* Property Bathrooms Parameter */
		if ( ( !empty( $_GET['bathrooms'] ) ) && ( $_GET['bathrooms'] != 'any' ) ) {
			$meta_query[] = array(
				'key'     => 'REAL_HOMES_property_bathrooms',
				'value'   => $_GET['bathrooms'],
				'compare' => '>=',
				'type'    => 'DECIMAL'
			);
		}
		if( isset( $_GET['price'] ) && $_GET['price'] !='' ){
			$price = explode( ',',$_GET['price'] );
			$min_price = $price[0];
			$max_price = $price[1];
			$meta_query[] = array(
				'key'     => 'price',
				'value'   => array( $min_price, $max_price ),
				'type'    => 'NUMERIC',
				'compare' => 'BETWEEN'
			);
		}

		/* if more than one taxonomies exist then specify the relation */
		$tax_count = count( $tax_query );
		if ( $tax_count > 1 ) {
			$tax_query['relation'] = 'AND';
		}

		/* if more than one meta query elements exist then specify the relation */
		$meta_count = count( $meta_query );
		if ( $meta_count > 1 ) {
			$meta_query['relation'] = 'AND';
		}

		if ( $tax_count > 0 ) {
			$search_args['tax_query'] = $tax_query;
		}

		/* if meta query has some values then add it to base home page query */
		if ( $meta_count > 0 ) {
			$search_args['meta_query'] = $meta_query;
		}
		return $search_args;
	}
}
add_filter( 'homes_filter_parameters', 'filter_search' );

function product_price($priceFloat) {
	$symbol = ' đ';
	$symbol_thousand = '.';
	$decimal_place = 0;
	$price = number_format($priceFloat, $decimal_place, '', $symbol_thousand);
	return $price.$symbol;
}