<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 9/9/2015
 * Time: 10:24 AM
 */
	get_header();
the_post();
?>
	<div id="primary" class="content-area">
	<div class="top-site-main">
		<div class="breadcrumbs-wrapper">
			<div class="container">
				<?php vifonic_breadcrumbs(); ?>
			</div>
		</div>
	</div>
	<div class="container">
			<main id="main" class="site-main" role="main">
				<div class="product-detail">
					<div class="top-product-content">
						<div class="entry-title">
							<h3><?php the_title(); ?></h3>
						</div>
						<div class="row">
							<div class="col-md-7 col-sm-12">
								<?php
									$gallery = get_field('gallery');
									if( $gallery ){
										?>
										<div id="slider-product" class="flexslider">
											<ul class="slides">
												<?php
													foreach( $gallery as $image_item ) :
												?>
												<li>
													<?php
														$image_attributes = $image_item['url'];
														$params = array('width' => 900,'height' => 700);
														$img = bfi_thumb($image_attributes, $params);
													?>
													<a class="retina" href="<?php echo $image_attributes ?>">
														<img src="<?php echo $img ?>" alt="<?php echo $image_item['alt']
														?>" />
													</a>
												</li>
												<?php
													endforeach;
												?>
											</ul>
										</div>
										<div id="carousel" class="flexslider">
											<ul class="slides">
												<?php
												foreach( $gallery as $image_item1 ) :
													?>
													<li>
														<img src="<?php echo $image_item1['sizes']['thumbnail'] ?>"
															 alt="<?php
														echo $image_item1['alt'] ?>" />
													</li>
												<?php
												endforeach;
												?>
											</ul>
										</div>
										<?php
									}
								?>
							</div>
							<div class="col-md-5 col-sm-12 product-info">
								<div class="product-status">
									<?php
										$status = get_field('product_status');
										$class_status = '';
										if( $status == '1' ){
											$status_text = 'Còn hàng';
											$class_status = 'in-stock';
										}elseif( $status == '2' ){
											$status_text = 'Hết hàng';
											$class_status = 'out-stock';
										}else{
											$status_text = 'Hàng sắp về';
											$class_status = 'coming-stock';
										}
									?>
									Tình trạng:
									<span class="<?php echo $class_status ?>" ><?php echo $status_text;
										?></span>
								</div>

								<p class="price">
									<?php
										if( get_field('price',$post->ID) ) {
											echo 'Giá: <span>' . product_price(get_field('price')) . ' </span>';
										}else{
											echo __('Giá: <span> Liên hệ</span>','vifonic');
										}
									?>
								</p>
								<hr />
								<div class="product-sales">
									<strong>Khuyến mãi:</strong>
									<p>
										<?php echo get_field('product_sale'); ?>
									</p>
								</div>
								<hr />
								<div class="comfortable_buy">
									<strong>Chế độ mua hàng</strong>
									<ul class="list-inline">
										<li>
											<i class="fa fa-bookmark-o"></i>
											<a href="#">Hàng chính hãng</a>
										</li>
										<li>
											<i class="fa fa-truck"></i>
											<a href="#">Vận chuyển miễn phí</a>
										</li>
										<li>
											<i class="fa fa-cogs"></i>
											<a href="#">Bảo hành tận nơi</a>
										</li>
										<li>
											<i class="fa fa-money"></i>
											<a href="#">Giá cả cạnh tranh</a>
										</li>
									</ul>
								</div>
								<div class="product-hotline">
									<hr />
									<p><i class="fa fa-phone"></i>Hotline: <span> <?php echo ot_get_option('holine') ?></span></p>
								</div>
								<div class="add-to-cat clearfix">
									<?php
										$args = array(
											'post_type' => 'page', /* or just 'post' */
											'meta_query' => array(
												array(
													'key' => '_wp_page_template',
													'value' => 'pagetemplate/template-shiping.php', /* your template file name */
													'compare' => '='
												)
											)
										);
										$pages = get_posts( $args );
										$link = get_permalink($pages[0]).'?productID='. $post->ID .'';
										wp_reset_postdata();
									?>
										<hr />
										<a class="showroom pull-left btn-success btn-lg" href="#">Show room</a>
										<a class="btn read-more btn-success btn-lg pull-right" href="<?php echo $link ?>">Đặt hàng</a>
								</div>
							</div>
						</div>
					</div>
					<div class="entry-content">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#detail1" aria-controls="detail1" role="tab"
																	  data-toggle="tab">Mô tả</a></li>
							<li role="presentation"><a href="#detail2" aria-controls="detail2" role="tab"
													   data-toggle="tab">Thông số</a></li>
							<li role="presentation"><a href="#detail4" aria-controls="detail4" role="tab"
													   data-toggle="tab">Hình ảnh - Video</a></li>
							<li role="presentation"><a href="#detail3" aria-controls="detail3" role="tab"
													   data-toggle="tab">Bình luận</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="detail1">
								<?php the_content(); ?>
							</div>
							<div role="tabpanel" class="tab-pane" id="detail2">
								<h3>THÔNG TIN CHUNG</h3>
								<?php
									$table1 = get_field('general');
									table_detail($table1);
								?>
								<h3>KIỂU DÁNG</h3>
								<?php
								$table1 = get_field('style');
								table_detail($table1);
								?>
								<h3>ĐẶC ĐIỂM</h3>
								<?php
								$table1 = get_field('teristics');
								table_detail($table1);
								?>
							</div>
							<div role="tabpanel" class="tab-pane" id="detail4">
								<?php the_field('image-video-product') ?>
							</div>
							<div role="tabpanel" class="tab-pane" id="detail3">
								<?php
								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
								?>
							</div>
						</div>
					</div>
				</div>
			</main>
	</div>

<?php get_footer(); ?>