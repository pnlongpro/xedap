<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vifonic
 */
$classed = array();
$classed[] = 'row post-in-excerpt';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classed); ?>>
	<?php if( has_post_thumbnail() ) {
		$class = 'col-xs-8'
	?>
	<div class="entry-thumbnail col-xs-4">
		<a class="post-thumbnail-link" href="<?php the_permalink() ?>" rel="bookmark">
			<figure class="post-thumbnail">
				<?php the_post_thumbnail('larger') ?>
			</figure>
		</a>
	</div>
	<?php
	}else {
		$class = 'col-xs-12';
	}
	?>
	<div class="entry-header-meta <?php echo $class ?>">
		<h3 class="entry-title">
			<a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
				<?php the_title() ?> </a>
		</h3>

		<div class="post-excerpt">
			<p><?php echo excerpt(50) ?></p>
		</div>
		<div class="post-meta clearfix">
			<div class="pull-left">
				<a href="<?php the_permalink() ?>" rel="nofollow" class="read-more">Xem chi tiết</a>
			</div>
			<div class="pull-right">
				<ul class="list-inline">
					<li>
						<p><i class="fa fa-clock-o"></i></p>
						<p><?php the_time('d, M, Y') ?></p>
					</li>

				</ul>
			</div>
		</div>
	</div>
</article>
<hr />