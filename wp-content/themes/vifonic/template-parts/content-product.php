<?php
$class = array();
$class[] = 'product-item';
?>
<div <?php echo post_class($class) ?>>
	<div class="product-thumnail">
		<a title="<?php the_title(); ?>" href="<?php the_permalink() ?>">
			<?php
			the_post_thumbnail('woocommerce_thumbnail')
			?>
		</a>
	</div>
	<div class="entry-content">
		<h3><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
		<?php
		if( get_field('price') ) {
			echo '<p class="price">Giá: <span>'. product_price(get_field('price')) .'</span></p>';
		}else{
			echo '<p class="price">Giá: <span>Liên hệ</span></p>';
		}
		?>
	</div>
	<div class="product-hover">
		<h3><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			<?php the_title(); ?></a></h3>
			<p><?= get_field('product_sale') ?></p>
		<p class="text-right">
			<a class="read-more btn btn-success" href="<?php the_permalink() ?>">Xem chi tiết</a>
		</p>

	</div>
</div>