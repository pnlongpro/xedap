<?php

$text = isset($_GET['code']) ? $_GET['code'] : 'Shortcode';
$font_size = 5;

$width = imagefontwidth($font_size) * strlen($text) + 20;
$height = imagefontheight($font_size) + 20;

$image = imagecreate($width, $height);
$background = imagecolorallocate($image, 241, 241, 241);
$textcolor = imagecolorallocate($image, 99, 99, 99);
$border = imagecolorallocate($image, 99, 99, 99);


imagestring($image, $font_size, 10, 10, $text, $textcolor);
imagerectangle($image , 0 , 0 , $width - 1, $height - 1, $border);

header('Content-type: image/png');

imagepng($image);
imagedestroy($image);
?>
