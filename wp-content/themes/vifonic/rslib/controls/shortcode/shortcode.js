(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);

( function($) {
	$(".lists-shortcode li").click(function(event){
		event.preventDefault();

		var id = $(this).attr('id');
		$(".lists-shortcode li").removeClass('active-tab');
		$(this).addClass('active-tab');
		
		$(".inner-content-shortcode").hide(); 
		$("#" + id + "-content").fadeIn(100);
		
		var str = $(this).find('a').text();
		$('.breadcrumb-shortcode p').html('<a>'+str+'</a>');
	});
	
	$('.close-shortcode, .rs-over-flow').click(function(event) {
		event.preventDefault();
		$('.rs-over-flow,.rs-shortcode-wrap').fadeOut(400);
	});
	
	// add shortcode
	$('.inner-content-shortcode form').submit(function(event){
		event.preventDefault();
		tinyMCE.triggerSave();
		
		var attr_shortcode = '';
		var content = '';
		var attr = $(this).serializeObject();
		
		$.each(attr,function(name){
			
			if( name == 'content' )
				content = this;
			else if( name == 'shortcode_name' )
				shortcode_name = this;
			else if($.isArray(this))
				attr_shortcode += ' ' + name + '="' + this.join('|') + '"';
			else 
				attr_shortcode += ' ' + name + '="' + this + '"';
			
		});
		
		shortcode = '['+ shortcode_name + attr_shortcode +']'+ content +'[/'+ shortcode_name +']';
		$('.rs-over-flow,.rs-shortcode-wrap').fadeOut(400);
		
		window.rs_shortcode_active_editor && window.rs_shortcode_active_editor.execCommand('mceInsertContent', 0, shortcode);
	});
	
    tinymce.PluginManager.add( 'rs_jquery', function( editor, url ) {
		$('.rs-over-flow').remove();
		$('body').append('<div class="rs-over-flow"></div>');
		
		//dont add button to editor if...
		var dont_add = $('.lists-shortcode .rs-menu-item').filter('.for-' + editor.settings.id + ',.for-all').length == 0;
		if(editor.settings.rs_shortcode_editor || dont_add) return false;
		
		//helper functions
		function getShortcodeList(id){
			var list = [];
			$('.lists-shortcode .rs-menu-item').filter('.for-' + id + ', .for-all').each(function(){
				list.push($(this).attr('id').replace('rs-shortcode-', ''));
			});
			return list;
		}
		
        function getAttr(s, n) {
            n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
            return n ?  window.decodeURIComponent(n[1]) : '';
        };
		
		function getImage(code, tag){
			return rs.lib.url + '/controls/shortcode/images/render.php?code=' + code;
		}
 
        function html(code, tag, data, con) {
            data = window.encodeURIComponent( data );
            con = window.encodeURIComponent( con ); 
			code = window.encodeURIComponent(code);
			var img = $('<img/>', {
				'src': getImage(code, tag), 
				'class': 'mceItem rs-shortcode-image ' + tag, 
				'data-rs-tag': tag,
				'data-rs-attr': data, 
				'data-rs-content' : con, 
				'data-mce-resize': false, 
				'title': 'Double click to edit',
				'data-mce-placeholder' : 1
			});
            return $('<div></div>').append(img).html();
        }
 
        function replaceShortcodes( content, list) {
			list = list.join('|');
			console.log(list);
			var reg = new RegExp('\\[('+list+')([^\\]]*)\\]([^\\]]*)\\[\\/('+list+')\\]', 'g');
            return content.replace(reg, function(all, tag, attr, con) {
                return html( all, tag, attr , con);
            });
        }
 
        function restoreShortcodes( content ) {
            return content.replace( /(?:<p(?: [^>]+)?>)*(<img [^>]+>)(?:<\/p>)*/g, function( match, image ) {
                var tag = getAttr( image, 'data-rs-tag' );
                var data = getAttr( image, 'data-rs-attr' );
                var con = getAttr( image, 'data-rs-content' );
 
                if ( data ) {
                    return '[' + tag + data + ']' + con + '[/' + tag + ']';
                }
                return match;
            });
        }
		
		//replace from shortcode to an image placeholder
        editor.on('BeforeSetcontent', function(event){
            //event.content = replaceShortcodes( event.content, getShortcodeList(event.target.id) );
        });
 
        //replace from image placeholder to shortcode
        editor.on('GetContent', function(event){
            //event.content = restoreShortcodes(event.content);
        });
		
		editor.on('DblClick',function(e) {
			if ( e.target.nodeName == 'IMG' && e.target.className.indexOf('rs-shortcode-image') > -1 ) {
				var data = e.target.attributes['data-rs-attr'].value;
				var content = e.target.attributes['data-rs-content'].value;
				var tag = e.target.attributes['data-rs-tag'].value;
				data = window.decodeURIComponent(data);
				content = window.decodeURIComponent(content);
				
				editor.execCommand('rs_shortcode_panel', '',{
                    data: data,
					content: content,
					tag: tag
                });
			}
		});
		
		//Add shortcode panel
		editor.addCommand('rs_shortcode_panel', function(ui, data) {
			var id = editor.settings.id;
				
			var length = $('.lists-shortcode .rs-menu-item').hide().filter('.for-' + id + ', .for-all').show().length;
							
			$('.rs-over-flow, .rs-shortcode-wrap').show();
			
			$(".inner-content-shortcode").hide().find('form').each(function(){
				this.reset();
			});
			$(".inner-content-shortcode").find('.rs-gallery-items .rs-gallery-delete,.rs-upload-delete').click();
			$(".inner-content-shortcode").find(':radio,:checkbox,select,input').trigger('change');
			
			var selection = editor.selection.getContent({format : 'text'}).trim();
			
			if(selection){
				var reg = new RegExp('^\\[(\\w[\\w\\d-_]*)([^\\]]*)\\]([^]*)\\[\\/([^\\]]*)\\]$', 'gm');
				reg.test();
				console.log(selection);
				var matches = reg.exec(selection);
				if(matches){
					data.tag = matches[1];
					data.data = matches[2];
					data.content = matches[3];
				}
			}
			
			if(data.tag){
				$(".lists-shortcode li#rs-shortcode-" + data.tag).click();
				$("#rs-shortcode-" + data.tag + "-content .box-content-shortcode").each(function(){
					
					var name = $(this).attr('id').replace('box-shortcode-', '');
					var value = name == 'content' ? data.content : getAttr(data.data, name);
					
					$(this).find('[name="'+name+'"]').not(':radio').val(value).trigger('change');	
					$(this).find('.rs-upload').has('[name="'+name+'"]').trigger('rs-upload-set', value);
					
					
					$(this).find('.wp-editor-area[name="'+name+'"]').each(function(){
						tinyMCE.get($(this).attr('id')).setContent(value);
					});
					
					try{
						$(this).find('[name="'+name+'"][value="'+value+'"]:radio').prop('checked', true).trigger('change');
						$(this).find('[name="'+name+'"][value="'+value+'"]:checkbox').prop('checked', true).trigger('change');
						$(this).find('[name="'+name+'[]"][value="'+value+'"]:checkbox').prop('checked', true).trigger('change');
					}
					catch(e){}

					if(value.indexOf('|') > 0 && name != 'content'){
						value = value.split('|');
						for(i in value){
							$(this).find('[name="'+name+'[]"]').not(':checkbox,select').eq(i).val(value[i]).trigger('change');
							try{
								$(this).find('[name="'+name+'[]"][value="'+value[i]+'"]:checkbox').prop('checked', true).trigger('change');
								$(this).find('[name="'+name+'[]"] option[value="'+value[i]+'"]').prop('selected', true).parent().trigger('change');
							}
							catch(e){
								console.log(e)
							}
						}
						$(this).find('.rs-gallery').has('[name="'+name+'"]').trigger('rs-gallery-set', value);
					}
					else{
						$(this).find('.rs-gallery').has('[name="'+name+'"]').trigger('rs-gallery-set', [value]);
					}
				});
			}
			else{
				$(".lists-shortcode li:visible:first").click();
			}
			
			$('.rs-shortcode-wrap').toggleClass('one-tab-only', length == 1);

		});
		
		// Add a button that opens a window
		editor.addButton( 'rs_button_key', {
			title: 'Theme Shortcode',
			icon: 'none dashicons-before dashicons dashicons-schedule',
			onclick: function() {
				editor.focus();
				window.rs_shortcode_active_editor = editor;
				
				editor.execCommand('rs_shortcode_panel', '',{
                    data: null,
					tag: null,
					content: null
                });
			}
		});
		
    });
	
	$(window).load(function(){
		tinyMCE.get('content').focus();
	});
	
})(jQuery);