<?php
/// RsSwitchButton Control - Render Script And HTML ////

class RsShortcode extends RsControl{
	static $shortcode = array();
	
	public $default = array(
		'name' => 'shortcode', // không đặt name = 'shortcode_name'
		'type' => 'shortcode', 
		'post_type' => 'post', // array( 'post', 'page')
		'editors' => 'all', // all | content | array('content', 'metabox-editor')
		'icon' => 'dashicons-marker',
		'title' => 'Shortcode Title',
		'controls' => array() // Nếu 1 control có name = content thì nó mặc định là content của shortcode, còn lại những name khác sẽ là attr
	);
	
	public function RsShortcode(){
		$this->addControl('shortcode', 'shortcode');
	}
	
	public function loadFiles(){
		rs::loadStyle('rs-shortcode', RS_LIB_URL . '/controls/shortcode/shortcode.min.css');
	}
	
	public function render($options = array()){
		$options = array_merge($this->default, $options);
		
		if($options['name']){
			$options['editors'] = (array) $options['editors'];
			static::$shortcode[$options['name']] = $options ;
			$this->loadFiles();			
		}
	}
	
	public static function renderControl($control){
		$not_allow = array('file', 'fileupload', 'repeater', 'group');
		if(in_array($control['type'], $not_allow)){
			echo $control['type'] . ' not support in shortcode.';
		}
		else{
			$control['render_by'] = 'rs_shortcode';
			if($control['type'] == 'editor'){
				$control['tinymce'] = array_merge(array('rs_shortcode_editor' => true), (array)$control['tinymce']);
			}
			$control['field_id'] = uniqid($control['name']);
			$return = rs::renderControl($control);
			if(rs::isMessage($return)){
				echo '<div class="rs-render-error">' . $return['message'] . '</div>';
			}
		}
	}
	
	public static function renderShortcode(){
		if(static::$shortcode){
			$array_shortcode = static::$shortcode;
		?>
		<div class="rs-shortcode-wrap" >
			<div class="content-shortcode-popup">
				<div class="main-lists-shortcode">
					<div class="lists-shortcode">
						<ul>
						<?php
							foreach($array_shortcode as $tab) {
							?>
							<li id="rs-shortcode-<?php echo esc_attr($tab['name']) ?>" class="rs-menu-item for-<?php echo implode(' for-', $tab['editors']) ?>">
								<?php if(!is_file($tab['icon'])) { ?>
									<i class="rs-icon dashicons <?php echo esc_attr($tab['icon']) ?>"></i>
								<?php } else { ?>
									<img src="<?php echo esc_attr($tab['icon']) ?>" class="rs-icon" alt=""/>
								<?php } ?>
								<a class="quotes" href="rs-shortcode-<?php echo esc_attr($tab['name']) ?>" data-tab-name="<?php echo esc_attr($tab['name']) ?>">
									<?php echo esc_attr($tab['title']) ?>
								</a>
							</li>
						<?php } ?>
						</ul>
					</div>
				</div>
				<div class="content-shortcode">
					<div class="breadcrumb-shortcode">
						<p></p>
						<a class="close-shortcode dashicons dashicons-no" title="close"></a>
					</div>
					<div class="container-content-shortcode">
						<?php foreach($array_shortcode as $tab) { ?>
						<div class="inner-content-shortcode for-<?php echo implode(' for-', $tab['editors']) ?>" id="rs-shortcode-<?php echo esc_attr($tab['name']) ?>-content">
							<form action="">
								<div class="main-content-shortcode">
									<?php if($tab['controls']) { ?>
									<?php foreach($tab['controls'] as $control) { ?>
									<!-- Control -->
									<div id="box-shortcode-<?php echo esc_attr($control['name']) ?>" class="box-content-shortcode">
										<div class="title-box-content-shortcode">
											<h4><?php echo force_balance_tags($control['label']) ?></h4>
											<p><?php echo force_balance_tags($control['description']) ?></p>
										</div>
										<div class="main-box-content-shortcode list-style-shortcode">
											<?php
												$control['conditional_logic_id'] = 'box-shortcode-'.$control['name'];
												static::renderControl($control);
											?>
										</div>
										<div class="clear"></div>
									</div>
									<!-- End Control -->
									<?php } ?>
									<?php } ?>
								</div>
								<input type="hidden" name="shortcode_name" value="<?php echo esc_attr($tab['name']) ?>" />
								<!-- Button Add Shortcode -->
								<div class="action-shortcode">
									<button type="submit" class="insert-shortcode"><i class="dashicons dashicons-yes"></i>Insert shortcode</button>
								</div>
							</form>
						</div><!--end content-tab-->
						<?php } ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<?php 
		}
	}
	
	public static function registerShortcode(){
		foreach(static::$shortcode as $shortcode){
			add_shortcode($shortcode['name'], $shortcode['callback']);
		}
	}
	
	public static function addTinyMce(){
		global $typenow;
		if(static::$shortcode){
			$add = false;
			foreach(static::$shortcode as $shortcode){
				if(in_array( $typenow, (array)$shortcode['post_type']) ){
					$add = true;
				}
			}
			
			if($add){
				add_filter( 'mce_external_plugins', array('RsShortcode', 'addTinyMcePlugin'));
				add_filter( 'mce_buttons', array('RsShortcode', 'addTinyMceButton') );
				foreach(static::$shortcode as &$shortcode)
					if( $shortcode['controls'] )
						foreach( $shortcode['controls'] as &$control)
							rs::preLoad($control['type']);
			}
		}
	}
	public static function addTinyMcePlugin( $plugin_array ) {
		$plugin_array['rs_jquery'] = RS_LIB_URL . '/controls/shortcode/shortcode.min.js';
		return $plugin_array;
	}

	public static function addTinyMceButton( $buttons ) {
		array_push( $buttons, 'rs_button_key' );
		return $buttons;
	}
}

add_action( 'admin_head', array('RsShortcode', 'addTinyMce'));
add_action( 'admin_footer', array('RsShortcode', 'renderShortcode'));
add_action( 'init', array('RsShortcode', 'registerShortcode'));
