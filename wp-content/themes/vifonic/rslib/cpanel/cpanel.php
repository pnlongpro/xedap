<?php
global $current_user;

rs::loadStyle('rs-panel-font', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
rs::loadStyle('rs-panel-css', RS_LIB_URL . '/cpanel/css/style.min.css');
rs::loadStyle('rs-panel-responsive-css', RS_LIB_URL . '/cpanel/css/reponsive.min.css');
rs::loadScript('rs-panel-script', RS_LIB_URL . '/cpanel/js/options.min.js');

$groups = array();

foreach(rs::$options as $tab){
	if($tab['controls']){
		$groups[$tab['name']] = $tab['controls'];
	}
	foreach($tab['subtabs'] as $subtab){
		if($subtab['controls']){
			$groups[$subtab['name']] = $subtab['controls'];
		}
	}
}

function rs_cpanel_render_control($control){
	$control['render_by'] = 'cpanel';
	$return = rs::renderControl($control);
	if(rs::isMessage($return)){
		echo '<div class="rs-render-error">' . $return['message'] . '</div>';
	}
}


?>
<script type="text/javascript">
	jQuery( window ).load(function() {
		if(jQuery.fn.rsSelectBox){
			setTimeout(function(){
				jQuery('.rs-googlefonts select').rsSelectBox();
			}, 2000);
		}
	});
</script>
<div class="wrap rs-panel-wrap">
	<h2 style="display:none"></h2>
	<div class="rs-panel">
		<div class="rs-header">
			<h2><?php _e('Theme Panel', 'rslib') ?></h2>
		</div>
		<div class="rs-content">
			<div class="rs-sidebar">
				<div class="rs-avatar">
					<?php echo get_avatar($current_user->user_email) ?>
					<p><?php _e('Welcome', 'rslib') ?>, <span><?php echo esc_html($current_user->display_name) ?></span></p>
					<div class="clear"></div>
				</div>
				<div class="rs-menu">
					<ul>
					<?php
						foreach(rs::$options as $tab){
							?>
							<li id="<?php echo esc_attr($tab['name']) ?>" class="rs-menu-item">								
								<a href="<?php echo rs::isUrl($tab['link']) ? $tab['link'] : '#'?>" data-tab-name="<?php echo esc_attr($tab['name']) ?>">
									<?php if(!is_file($tab['icon'])) { ?>
										<i class="rs-icon dashicons <?php echo esc_attr($tab['icon']) ?>"></i>
									<?php } else { ?>
										<img src="<?php echo esc_url($tab['icon']) ?>" class="rs-icon" alt=""/>
									<?php } ?>
									<?php echo force_balance_tags($tab['title']) ?>
								</a>
								<?php
									if(count($tab['subtabs'])){
									?>
									<ul>
										<?php
										foreach($tab['subtabs'] as $subtab){
											?>
											<li id="<?php echo esc_attr($subtab['name']) ?>" class="rs-menu-sub-item">
												<a href="<?php echo rs::isUrl($subtab['link']) ? esc_url($subtab['link']) : '#'?>" data-tab-name="<?php echo esc_attr($subtab['name']) ?>"><?php echo force_balance_tags($subtab['title']) ?></a>
											</li>
											<?php
										}
										?>
									</ul>
									<?php
									}
								?>
							</li>
							<?php
						}
					?>
					</ul>               
				</div>
			</div><!--end rs-sidebar-->
			<div class="rs-content-main">
				<div class="rs-breadcrumb">
					<ul>
						<li class="rs-breadcrumb-e1"><a href="#">Theme panel</a></li>
						<li class="rs-breadcrumb-e2"><a href="#">UI Elements</a></li>
						<li class="rs-breadcrumb-e3"><a href="#">General</a></li>
					</ul>
				</div><!--end rs-breadcrumb-->
				<div class="rs-form">
					<form action="<?php echo admin_url('/themes.php?page=theme-options') ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="rs-options" value="true"/>
						<?php
							$rscontrol = new RsControl;
							foreach($groups as $name=>$fields) { ?>
								<div class="rs-fields" id="rs-tab-<?php echo esc_attr($name) ?>"> 
									<?php foreach($fields as $field) { 
										if($field['type'] == 'script') {
											rs::renderControl($field);
										}
										else{
											if(null !== $value = $rscontrol->getOption($field['name'])){
												$field['value'] = $value;
											}
											$field = $rscontrol->parseOptions($field);
											if($field['name'] == 'noname'){
												unset($field['name']);
												$field['label'] = '';
											}
											$field['conditional_logic_id'] = $field['field_id'] . '-field';
											?>
											<div class="rs-field" id="<?php echo esc_attr($field['conditional_logic_id']) ?>">
												<div class="rs-field-label">
													<label for="<?php echo esc_attr($field['field_id']) ?>"><?php echo force_balance_tags($field['label']) ?></label>
													<p><?php echo force_balance_tags($field['description']) ?></p>
												</div>
												<div class="rs-field-editor">
													<?php rs_cpanel_render_control($field) ?>
												</div>
												<div class="clear"></div>
											</div>
											<?php 
										} 
									}?>
								</div>
							<?php } 
						?>						
						<div class="rs-form-action">
							<input type="submit" class="rs-button rs-button-primary" value="Save changes" name="save" />
							<span class="spinner"></span>
							<span class="rs-action-msg"></span>
						</div>
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>