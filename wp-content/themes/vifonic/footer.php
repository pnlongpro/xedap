<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vifonic
 */

?>
</div><!--end container-->
</section><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">

	<div class="top-footer ">
		<div class="container">
			<div class="row">
				<?php
				if ( is_active_sidebar( 'sidebar-2' ) ) {
					dynamic_sidebar( 'sidebar-2' );
				}
				?>
			</div>
		</div>
	</div><!--end top-footer-->

	<div class="site-info">
		<div class="container">
			<p>Copyright &#169; 2015 xediennikyo.com website. All Rights Reserved. Designed by <a href="http://vifonic.com" target="_blank" title="Thiết kế website">Vifonic</a></p>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<?php echo ot_get_option('tracking_code')  ?>

</body>
</html>
