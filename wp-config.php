<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'xedap');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f|TI7nT?$oSP%[%j[e~`L-+_<@.i7`UCFmh*fK&h?o-;?Q*K)5BH<o)vT(5z -=8');
define('SECURE_AUTH_KEY',  'X>[]!fXdth$.2g4[D*|/Y FvsrT0`/+EnV`c~a9P!r@^~mTj4*lO&w9riTtci%HK');
define('LOGGED_IN_KEY',    '=]22Oh=F9jH593AvAUJB~$JE<3&A>c[9{*F7h*h~u Tf.:|?(U*Xz@R-ZD+T7<C ');
define('NONCE_KEY',        ';nfTU,p`bFB8b ed|Gp7Qli|d|LX|tIZS}#lv403mwX!$Q5f^.-!WhN2NA:^O-Qo');
define('AUTH_SALT',        'Ge^@miP>tvhU%QSZ#?J|+wP5f)zlD05i3[(9)0H;e|&.<B:sB-Im)+LlCXc2}O[x');
define('SECURE_AUTH_SALT', 'g4EJI@eYkHH]y5$WJ)E4Q{rP|o68R,r(^Z!oOd,qWI7H|x`Pnx*:iX(]Obd>Wrau');
define('LOGGED_IN_SALT',   '-p|zmg/LHE^fT48Q+H&cGaoY3#kj6=_z!pH=HqMh1<!X|,}kyOg^$h3BkyI2+pi*');
define('NONCE_SALT',       '@yPL|i7]6oBbm-*)PH:r7hVl4G54O #wrccl5QE6q*}!{]uVblT[?+l:b)+5ea^N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'xd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
